<?php

declare(strict_types=1);

namespace Drupal\a12s_locations\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a12s_locations_map_provider annotation object.
 *
 * @Annotation
 */
final class A12sLocationsMapProvider extends Plugin {

  /**
   * The plugin ID.
   */
  public readonly string $id;

  /**
   * The human-readable name of the plugin.
   *
   * @ingroup plugin_translatable
   */
  public readonly string $title;

  /**
   * The description of the plugin.
   *
   * @ingroup plugin_translatable
   */
  public readonly string $description;

}
