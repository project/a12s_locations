<?php

declare(strict_types=1);

namespace Drupal\a12s_locations\Plugin;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Base class for a12s_locations_map_provider plugins.
 */
abstract class MapProviderPluginBase extends PluginBase implements MapProviderInterface {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function label(): string {
    // Cast the label to a string since it is a TranslatableMarkup object.
    return (string) $this->pluginDefinition['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildDrupalSettings(string $cssSelector): array {
    return [
      'selector' => $cssSelector,
      'provider' => $this->getPluginId(),
      'options' => [
        'map' => $this->buildMapOptions(),
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function validateSettingsForm(array $form, FormStateInterface $formState): void {}

  /**
   * {@inheritdoc}
   */
  public function submitSettingsForm(array $form, FormStateInterface $formState): void {}

}
