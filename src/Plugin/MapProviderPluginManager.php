<?php

declare(strict_types=1);

namespace Drupal\a12s_locations\Plugin;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\a12s_locations\Annotation\A12sLocationsMapProvider;

/**
 * A12sLocationsMapProvider plugin manager.
 *
 * @method \Drupal\a12s_locations\Plugin\MapProviderInterface createInstance(string $plugin_id, array $configuration = [])
 */
final class MapProviderPluginManager extends DefaultPluginManager {

  /**
   * Constructs the object.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/A12sLocationsMapProvider', $namespaces, $module_handler, MapProviderInterface::class, A12sLocationsMapProvider::class);
    $this->alterInfo('a12s_locations_map_provider_info');
    $this->setCacheBackend($cache_backend, 'a12s_locations_map_provider_plugins');
  }

}
