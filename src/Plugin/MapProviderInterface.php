<?php

declare(strict_types=1);

namespace Drupal\a12s_locations\Plugin;

use Drupal\Component\Plugin\DerivativeInspectionInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Interface for a12s_locations_map_provider plugins.
 */
interface MapProviderInterface extends PluginInspectionInterface, DerivativeInspectionInterface {

  /**
   * Returns the translated plugin label.
   */
  public function label(): string;

  /**
   * Retrieve a list of libraries to attach to the page.
   *
   * @return array
   *   An array containing a list of libraries.
   */
  public function getLibraries(): array;

  /**
   * Build the Drupal settings for a specific CSS selector.
   *
   * This is to be used in the "#attached" property of a render array.
   *
   * @param string $cssSelector The CSS selector of the element.
   *
   * @return array
   */
  public function buildDrupalSettings(string $cssSelector): array;

  /**
   * Builds the options for a map.
   *
   * @return array The map options.
   */
  public function buildMapOptions(): array;

  /**
   * Get the default values.
   *
   * @return array
   *   The default values.
   */
  public function defaultValues(): array;

  /**
   * Build the settings form.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The form state.
   * @param array $settings
   *   The stored settings.
   *
   * @return array
   */
  public function settingsForm(array $form, FormStateInterface $formState, array $settings = []): array;

  /**
   * Settings subform validation handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The current state of the form.
   */
  public function validateSettingsForm(array $form, FormStateInterface $formState): void;

  /**
   * Settings subform submit handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The current state of the form.
   */
  public function submitSettingsForm(array $form, FormStateInterface $formState): void;

}
