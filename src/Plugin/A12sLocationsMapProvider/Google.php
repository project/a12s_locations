<?php

declare(strict_types=1);

namespace Drupal\a12s_locations\Plugin\A12sLocationsMapProvider;

use Drupal\a12s_locations\Plugin\MapProviderPluginBase;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Plugin implementation of the a12s_locations_map_provider.
 *
 * @A12sLocationsMapProvider(
 *   id = "google",
 *   label = @Translation("Google map"),
 *   description = @Translation("Integrates with Google map API.")
 * )
 */
class Google extends MapProviderPluginBase {

  const LAT_LNG_REGEX = '/^(?<latitude>-?(?:\d*\.)?\d+),(?<longitude>-?(?:\d*\.)?\d+)$/';

  public function getLibraries(): array {
    return [
      'a12s_locations/google-map',
      'a12s_locations/google-map-marker-clusterer',
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function defaultValues(): array {
    return [
      'provider' => [
        'mapTypeId' => 'ROADMAP',
      ],
      'style' => '',
      'center' => NULL,
      'initialZoom' => 6,
      'minZoom' => 1,
      'maxZoom' => 22,
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function buildMapOptions(): array {
    $options = [];

    foreach (array_keys($this->defaultValues()) as $key) {
      if (!empty($this->configuration[$key])) {
        switch ($key) {
          case 'style':
            $options[$key] = json_decode($this->configuration[$key]);
            break;

          case 'center':
            $matches = [];

            if (preg_match(self::LAT_LNG_REGEX, $this->configuration[$key], $matches)) {
              $options[$key] = [
                'lat' => (float) $matches['latitude'],
                'lng' => (float) $matches['longitude'],
              ];
            }
            break;

          case 'initialZoom':
          case 'minZoom':
          case 'maxZoom':
            $options[$key] = (int) $this->configuration[$key];
            break;

          default:
            $options[$key] = $this->configuration[$key];

        }
      }
    }

    return $options;
  }

  /**
   * {@inheritDoc}
   */
  public function settingsForm(array $form, FormStateInterface $formState, array $settings = []): array {
    $settings = array_replace_recursive($this->defaultValues(), $settings);

    $form['provider']['mapTypeId'] = [
      '#type' => 'select',
      '#title' => $this->t('Map type'),
      '#options' => [
        'ROADMAP' => $this->t('Roadmap'),
        'SATELLITE' => $this->t('Satellite'),
        'HYBRID' => $this->t('Hybrid'),
        'TERRAIN' => $this->t('Terrain'),
      ],
      '#default_value' => NestedArray::getValue($settings, ['provider', 'mapTypeId']),
      '#required' => TRUE,
    ];

    $form['style'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Map style'),
      '#description' => $this->t('The map style for Google Map uses JSON format. You can use the @url to build custom styles.', [
        '@url' => Link::fromTextAndUrl($this->t('Snazzy maps'), Url::fromUri('http://snazzymaps.com/'))->toString(),
      ]),
      '#default_value' => NestedArray::getValue($settings, ['style']),
    ];

    $form['center'] = [
      '#type' => 'textfield',
      '#title' => $this->t('The coordinates of the initial map center'),
      '#description' => $this->t('The expected format is "latitude,longitude". For example: <em>3.844119,11.501346</em>.'),
      '#default_value' => NestedArray::getValue($settings, ['center']),
    ];

    $form['initialZoom'] = [
      '#type' => 'range',
      '#title' => $this->t('The initial zoom level'),
      '#min' => 1,
      '#max' => 22,
      '#default_value' => NestedArray::getValue($settings, ['initialZoom']),
    ];

    $form['minZoom'] = [
      '#type' => 'range',
      '#title' => $this->t('The minimum allowed zoom level'),
      '#min' => 1,
      '#max' => 22,
      '#default_value' => NestedArray::getValue($settings, ['minZoom']),
    ];

    $form['maxZoom'] = [
      '#type' => 'range',
      '#title' => $this->t('The maximum allowed zoom level'),
      '#min' => 1,
      '#max' => 22,
      '#default_value' => NestedArray::getValue($settings, ['maxZoom']),
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function validateSettingsForm(array $form, FormStateInterface $formState): void {
    $style = trim($formState->getValue('style', ''));
    $formState->setValue('style', $style);

    if ($style) {
      // @todo use json_validate() when PHP ≥ 8.3 usage is more important.
      json_decode($style);

      if (json_last_error() !== JSON_ERROR_NONE) {
        $formState->setError($form['style'], $this->t('The provided style is not a valid JSON string.'));
      }
    }

    $center = trim($formState->getValue('center', ''));
    $formState->setValue('center', $center);

    if ($center) {
      $matches = [];

      if (!preg_match(self::LAT_LNG_REGEX, $center, $matches)) {
        $formState->setError($form['center'], $this->t('The provided center is not valid coordinates.'));
      }
      else {
        if (abs((float) $matches['latitude']) > 90) {
          $formState->setError($form['center'], $this->t('The latitude is in a range between -90 and +90.'));
        }
        elseif (abs((float) $matches['longitude']) > 180) {
          $formState->setError($form['center'], $this->t('The longitude is in a range between -180 and +180.'));
        }
      }
    }

    $initialZoom = trim((string) $formState->getValue('initialZoom'));
    $minZoom = trim((string) $formState->getValue('minZoom'));
    $maxZoom = trim((string) $formState->getValue('maxZoom'));

    if ($minZoom && $maxZoom && $maxZoom < $minZoom) {
      $formState->setError($form['minZoom'], $this->t('The minimum zoom level cannot be greater than the maximum zoom level.'));
    }
    elseif ($initialZoom && $minZoom && $minZoom > $initialZoom) {
      $formState->setError($form['initialZoom'], $this->t('The initial zoom level cannot be lower than the minimum zoom level.'));
    }
    elseif ($initialZoom && $maxZoom && $maxZoom < $initialZoom) {
      $formState->setError($form['initialZoom'], $this->t('The initial zoom level cannot be greater than the maximum zoom level.'));
    }
  }

}
