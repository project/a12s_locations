<?php

declare(strict_types=1);

namespace Drupal\a12s_locations\Plugin\A12sLocationsMapProvider;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Plugin implementation of the a12s_locations_map_provider.
 *
 * @A12sLocationsMapProvider(
 *   id = "baidu",
 *   label = @Translation("Baidu map"),
 *   description = @Translation("Integrates with Baidu map API.")
 * )
 */
class Baidu extends Google {

  public function getLibraries(): array {
    return [
      'a12s_locations/baidu-map',
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function defaultValues(): array {
    return [
      'style' => '',
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function buildMapOptions(): array {
    $options = [];

    if (!empty($this->configuration['style'])) {
      $options['style'] = json_decode($this->configuration['style']);
    }

    return $options;
  }

  /**
   * {@inheritDoc}
   */
  public function settingsForm(array $form, FormStateInterface $formState, array $settings = []): array {
    $settings = array_replace_recursive($this->defaultValues(), $settings);

    $form['style'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Map style'),
      '#description' => $this->t('The map style for Baidu Map uses JSON format. You can use the @url to build custom styles.', [
        '@url' => Link::fromTextAndUrl($this->t('personalized map style editor'), Url::fromUri('https://lbsyun.baidu.com/index.php?title=open/custom'))->toString(),
      ]),
      '#default_value' => NestedArray::getValue($settings, ['style']),
    ];

    return $form;
  }

}
