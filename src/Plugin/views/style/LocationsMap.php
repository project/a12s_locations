<?php

namespace Drupal\a12s_locations\Plugin\views\style;

use Drupal\a12s_locations\Form\MapConfigTrait;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Database\StatementInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Template\Attribute;
use Drupal\search_api\Query\ResultSetInterface;
use Drupal\views\Plugin\views\style\HtmlList;
use Drupal\views\ResultRow;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides Views plugin for building a locations map.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "a12s_locations_map",
 *   title = @Translation("Locations map"),
 *   help = @Translation("Dipslays locations on a map."),
 *   theme = "views_view_a12s_locations_map",
 *   display_types = {"normal"}
 * )
 */
class LocationsMap extends HtmlList {

  use MapConfigTrait;

  const REQUIRED_MAPPING_FIELDS = [
    'id',
    'title',
    'coordinates',
  ];

  const ZOOM_ON_LOCATION_CLICK_DEFAULT = 14;

  const CACHED_DATA_FOLDER = 'private://a12s_locations/cache';

  /**
   * Stores the location IDs to be used for AJAX response.
   *
   * @var array
   */
  protected array $ajaxResponseLocationIds = [];

  /**
   * Whether the cache is being rebuilt.
   *
   * @var bool
   */
  protected bool $cacheRebuilding = FALSE;

  /**
   * Do not use grouping.
   *
   * @var bool
   */
  protected $usesGrouping = FALSE;

  /**
   * {@inheritdoc}
   */
  protected $usesRowPlugin = TRUE;

  /**
   * Does the style plugin support custom css class for the rows.
   *
   * @var bool
   */
  protected $usesRowClass = TRUE;

  /**
   * Use fields without a row plugin.
   *
   * @var bool
   */
  protected $usesFields = TRUE;

  /**
   * {@inheritdoc}
   *
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   The language manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *    The module handler;
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    protected LanguageManagerInterface $languageManager,
    protected EntityTypeManagerInterface $entityTypeManager,
    protected ModuleHandlerInterface $moduleHandler
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   *
   * @noinspection PhpParamsInspection
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): static {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('language_manager'),
      $container->get('entity_type.manager'),
      $container->get('module_handler')
    );
  }

  public function isCacheRebuilding(): bool {
    return $this->cacheRebuilding;
  }

  public function setCacheRebuilding(bool $cacheRebuilding): void {
    $this->cacheRebuilding = $cacheRebuilding;
  }

  /**
   * Get the file path of cached data.
   *
   * @param string $viewId
   *    The view ID.
   * @param string $displayId
   *    The view display ID.
   * @param array $variant
   *
   * @return string
   */
  public static function getCacheFilePath(string $viewId, string $displayId, array $variant): string {
    $parts = [$displayId];

    foreach ($variant as $key => $value) {
      $parts[] = "$key-$value";
    }

    $suffix = str_replace(':', '--', implode('--', $parts));
    return self::CACHED_DATA_FOLDER . "/{$viewId}/{$suffix}.html";
  }

  public function getAjaxResponseLocationIds(): ?array {
    return $this->ajaxResponseLocationIds;
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions(): array {
    $options = parent::defineOptions();

    $options['provider'] = ['default' => ''];

    $options['mapping'] = [
      'contains' => [
        'id' => ['default' => ''],
        'title' => ['default' => ''],
        'coordinates' => ['default' => ''],
        'description' => ['default' => []],
        'data' => ['default' => []],
      ],
    ];

    $options['options'] = [
      'contains' => [
        'zoom_on_location_click' => ['default' => self::ZOOM_ON_LOCATION_CLICK_DEFAULT],
        'hide_not_visible_location' => ['default' => false],
      ],
    ];

    $options['cache'] = [
      'contains' => [
        'enable' => ['default' => FALSE],
      ],
    ];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state): void {
    $fieldLabels = $this->displayHandler->getFieldLabels();
    $form += $this->mapProviderFormElement($this->options['provider'] ?? '', 'provider');

    $form['mapping'] = [
      '#type' => 'details',
      '#title' => $this->t('Fields mapping'),
      '#open' => count(self::REQUIRED_MAPPING_FIELDS) > count($this->getRequiredMappingFields()),
    ];

    $form['mapping']['contains']['id'] = [
      '#type' => 'select',
      '#title' => $this->t('Identifier'),
      '#description' => $this->t('A unique identifier for the location.'),
      '#options' => $fieldLabels,
      '#required' => TRUE,
      '#default_value' => $this->getMappingOption('id'),
    ];

    $form['mapping']['contains']['title'] = [
      '#type' => 'select',
      '#title' => $this->t('Title'),
      '#description' => $this->t('The location title.'),
      '#options' => $fieldLabels,
      '#required' => TRUE,
      '#default_value' => $this->getMappingOption('title'),
    ];

    $form['mapping']['contains']['coordinates'] = [
      '#type' => 'select',
      '#title' => $this->t('Coordinates'),
      '#description' => $this->t('The location coordinates, which should be in the %format format.', ['%format' => 'lat, lon']),
      '#options' => $fieldLabels,
      '#required' => TRUE,
      '#default_value' => $this->getMappingOption('coordinates'),
    ];

    $form['mapping']['contains']['description'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Description'),
      '#description' => $this->t('Select one or more fields to be displayed as the location description.'),
      '#options' => $fieldLabels,
      '#multiple' => TRUE,
      '#default_value' => $this->getMappingOption('description'),
    ];

    // @todo use a 'table' element to also define the property name for each
    //   field.

    $form['mapping']['contains']['data'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Data'),
      '#description' => $this->t('Data fields are not displayed but are made available for JavaScript processing.'),
      '#options' => $fieldLabels,
      '#multiple' => TRUE,
      '#default_value' => $this->getMappingOption('data'),
    ];

    $form['options'] = [
      '#type' => 'details',
      '#title' => $this->t('Options'),
      '#open' => FALSE,
    ];

    $form['options']['contains']['hide_not_visible_location'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Hide location when not visible on map'),
      '#description' => $this->t('By default, all the locations are always visible on the sidebar. When this setting is enabled, only the locations currently visible on the map are shown.'),
      '#default_value' => $this->getLocationMapOption('hide_not_visible_location'),
    ];

    $range = range(1, 22);

    $form['options']['contains']['zoom_on_location_click'] = [
      '#type' => 'select',
      '#title' => $this->t('Zoom level used when a location is clicked'),
      '#options' => array_combine($range, $range),
      '#empty_option' => $this->t('Ignore'),
      '#default_value' => $this->getLocationMapOption('zoom_on_location_click'),
    ];

    $form['cache'] = [
      '#type' => 'details',
      '#title' => $this->t('Data caching'),
      '#open' => FALSE,
    ];

    $form['cache']['description'] = [
      '#markup' => $this->t('The data cache may be necessary when dealing with a big amount of locations, without pager. This ensures that the locations are quickly displayed on the map. But such caching is not compatible with view arguments and requires AJAX, so you need to satisfy those requirements before being able to enable caching.'),
    ];

    // @todo add description and constraints (require AJAX), options (empty
    //   message if cache not generated?), validation (incompatible with views
    //   args)...

    $useAjax = $this->view->display_handler->ajaxEnabled();
    $hasArguments = !empty($this->view->display_handler->getOption('arguments'));

    $form['cache']['contains']['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable caching'),
      '#default_value' => $this->getCacheOption('enabled'),
      '#disabled' => !$useAjax || $hasArguments,
    ];

    parent::buildOptionsForm($form, $form_state);
    unset($form['type']);
  }

  /**
   * Render the display in this style.
   */
  public function render(): array {
    if (!($mapProviderPlugin = $this->getPluginFromMapConfigId($this->options['provider'] ?? NULL))) {
      return [];
    }

    $requiredMapFields = $this->getRequiredMappingFields();
    $logger = \Drupal::logger('a12s_locations');
    $tArgs = [
      '%name' => $this->view->id(),
      '%display_id' => $this->view->current_display,
    ];

    if ($missingMapFields = array_diff(LocationsMap::REQUIRED_MAPPING_FIELDS, array_keys($requiredMapFields))) {
      $logger->error('The required properties %list are not mapped with a field in the view %name:%display_id, the locations cannot be processed.', [
          '%list' => implode(', ', $missingMapFields),
        ] + $tArgs);
      return [];
    }

    // Handle AJAX.
    $route = \Drupal::request()->attributes->get('_route');
    if ($route === 'views.ajax') {
      // The AJAX response is altered to return those IDs.
      // @see \Drupal\a12s_locations\EventSubscriber\ViewsAjaxEventSubscriber
      $this->ajaxResponseLocationIds = $this->getLocationIds($requiredMapFields['id']);
      // No need for any rows content.
      return [];
    }

    $descriptionFields = array_filter($this->getMappingOption('description', []));
    array_unshift($descriptionFields, $requiredMapFields['title']);
    $dataFields = array_filter($this->getMappingOption('data', []));

    $className = "a12s-locations-map-{$this->view->dom_id}";
    $this->view->element['#attached']['library'][] = 'a12s_locations/location-map';

    // @todo add an option for enabling clusters.
    foreach ($mapProviderPlugin->getLibraries() as $library) {
      $this->view->element['#attached']['library'][] = $library;
    }

    $settings = $mapProviderPlugin->buildDrupalSettings('.' . $className);
    $settings['options']['hideNotVisibleLocations'] = (bool) $this->getLocationMapOption('hide_not_visible_location', FALSE);

    if ($zoom = $this->getLocationMapOption('zoom_on_location_click', FALSE)) {
      $settings['options']['zoomOnLocationClick'] = (int) $zoom;
    }

    $this->view->element['#attached']['drupalSettings']['a12sLocations']['locationMaps'][$this->view->dom_id] = $settings;
    $cssClass = $this->view->display_handler->getOption('css_class');
    $this->view->display_handler->setOption('css_class', "$cssClass $className");

    if (!$this->isCacheRebuilding() && $this->getCacheOption('enabled')) {
      $variant = ['languages:' . LanguageInterface::TYPE_INTERFACE => $this->languageManager->getCurrentLanguage()->getId()];
      $context = [
        'view_id' => $this->view->id(),
        'displays' => $this->view->current_display,
      ];

      \Drupal::moduleHandler()->alter('a12s_locations_map_cache_current_variant', $variant, $context);
      $filePath = self::getCacheFilePath($this->view->id(), $this->view->current_display, $variant);

      if (!file_exists($filePath)) {
        // @todo provide some fallbacks.
        return [];
      }

      // @todo add this option and update JS script to set the browser history.
      if ($this->getCacheOption('use_uri', FALSE)) {
        if ($ids = $this->renderSelectedLocations($requiredMapFields['id'])) {
          $this->view->element['#attached']['drupalSettings']['a12sLocations']['locationMaps'][$this->view->dom_id]['enabledLocations'] = $ids;
        }
      }

      // @todo provide a lazy placeholder?
      return [
        '#markup' => Markup::create(file_get_contents($filePath)),
      ];
    }

    $output = parent::render();
    $rows = $output[0]['#rows'];
    unset($output[0]['#rows']);

    foreach ($rows as $id => $row) {
      /** @var \Drupal\views\ResultRow $resultRow */
      $resultRow = $row['#row'];
      $values = [];

      foreach ($requiredMapFields as $key => $field) {
        $fieldOutput = $this->getField($resultRow->index, $field);
        $values[$key] = $fieldOutput !== NULL ? trim(strip_tags($fieldOutput)) : '';

        if ($values[$key] === '') {
          unset($rows[$id]);
          continue 2;
        }
      }

      // The location list should only use the fields specified in the
      // "description" mapping.
      $this->view->field = array_intersect_key($this->view->field, array_flip($descriptionFields));

      // Add extra data fields to the data attribute.
      foreach ($dataFields as $field) {
        $fieldOutput = $this->getField($resultRow->index, $field);
        $values[$field] = $fieldOutput !== NULL ? trim(strip_tags($fieldOutput)) : '';
      }

      $listItemAttribute = new Attribute(['data-a12s-locations-point' => Json::encode($values)]);

      if ($rowClass = $this->getRowClass($id)) {
        $listItemAttribute->addClass($rowClass);
      }

      $item = [
        'content' => $row,
        '#prefix' => '<li' . $listItemAttribute . '>',
        '#suffix' => '</li>',
      ];

      $output[0]['#rows'][] = $item;
      unset($rows[$id]);
    }

    return $output;
  }

  public function getMappingOption(string $name, mixed $default = NULL): mixed {
    return $this->getNestedOption('mapping', $name, $default);
  }

  public function getLocationMapOption(string $name, mixed $default = NULL): mixed {
    return $this->getNestedOption('options', $name, $default);
  }

  public function getCacheOption(string $name, mixed $default = NULL): mixed {
    return $this->getNestedOption('cache', $name, $default);
  }

  public function getNestedOption(string $group, string $name, mixed $default = NULL): mixed {
    return $this->options[$group]['contains'][$name] ?? $default;
  }

  public function getRequiredMappingFields(): array {
    return array_reduce(self::REQUIRED_MAPPING_FIELDS, function(array $fields, string $key) {
      if ($field = $this->getMappingOption($key)) {
        $fields[$key] = $field;
      }

      return $fields;
    }, []);
  }

  protected function renderSelectedLocations(string $idField): array {
    // Get the total results, excluding exposed filters.
    $viewClone = clone $this->view;
    $viewClone->storage = $viewClone->storage->createDuplicate();
    $viewClone->executed = FALSE;
    $viewClone->query = NULL;
    unset($viewClone->display_handler, $viewClone->current_display, $viewClone->build_info['query']);

    $cloneDefaultDisplay = &$viewClone->storage->getDisplay('default');
    $cloneDisplay = &$viewClone->storage->getDisplay($this->view->current_display);

    foreach ([$cloneDefaultDisplay, $cloneDisplay] as &$display) {
      foreach ($display['display_options']['filters'] as &$filter) {
        $filter['exposed'] = FALSE;
      }
    }

    $viewClone->setDisplay($this->view->current_display);
    $viewClone->initQuery();
    $countQuery = $viewClone->getQuery()->query(TRUE);
    $countQuery->preExecute();
    $result = $countQuery->execute();
    $total = NULL;

    if ($result instanceof ResultSetInterface) {
      $total = (int) $result->getResultCount();
    }
    elseif ($result instanceof StatementInterface) {
      $total = (int) $result->fetchField();
    }

    if ($total && count($this->view->result) !== $total) {
      return $this->getLocationIds($idField);
    }

    return [];
  }

  protected function getLocationIds(string $idField) {
    $isSearchApiResult = !empty($this->view->result) && $this->moduleHandler->moduleExists('search_api') && is_a(reset($this->view->result), 'Drupal\\search_api\\Plugin\\views\\ResultRow', TRUE);

    return array_reduce($this->view->result, function($rows, ResultRow $row) use ($idField, $isSearchApiResult) {
      if (isset($this->view->field[$idField]->realField)) {
        $key = $this->view->field[$idField]->realField;

        if ($value = $row->{$key}) {
          $rows[] = $isSearchApiResult ? reset($value) : $value;
        }
      }

      return $rows;
    }, []);
  }

}
