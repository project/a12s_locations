<?php

namespace Drupal\a12s_locations\EventSubscriber;

use Drupal\a12s_locations\Ajax\LocationsMapUpdateCommand;
use Drupal\a12s_locations\Plugin\views\style\LocationsMap;
use Drupal\views\Ajax\ViewAjaxResponse;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Handle AJAX response from views and transform it if related to locations map.
 */
class ViewsAjaxEventSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[KernelEvents::RESPONSE] = ['onKernelResponse', 128];
    return $events;
  }

  /**
   * Alter the view AJAX response.
   */
  public function onKernelResponse(ResponseEvent $event): void {
    $response = $event->getResponse();

    if ($response instanceof ViewAjaxResponse && $event->isMainRequest()) {
      $view = $response->getView();
      $stylePlugin = $view->style_plugin;

      if ($stylePlugin instanceof LocationsMap) {
        $ids = $stylePlugin->getAjaxResponseLocationIds();
        $commands = &$response->getCommands();
        $commands = [];
        $response->addAttachments(['library' => ['a12s_locations/map-update-ajax']]);
        $response->addCommand(new LocationsMapUpdateCommand(".a12s-locations-map-{$view->dom_id}", $ids));

        // @todo display empty message.
        //if (!$ids) {
        //
        //}
      }
    }
  }

}
