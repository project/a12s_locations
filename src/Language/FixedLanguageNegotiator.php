<?php

namespace Drupal\a12s_locations\Language;

use Drupal\language\LanguageNegotiator;

/**
 * Allow to force current language to a specific one, for batch processing.
 */
class FixedLanguageNegotiator extends LanguageNegotiator {

  protected ?string $languageCode = NULL;

  /**
   * {@inheritdoc}
   */
  public function initializeType($type): array {
    $methodId = static::METHOD_ID;
    $availableLanguages = $this->languageManager->getLanguages();

    if ($this->languageCode && isset($availableLanguages[$this->languageCode])) {
      $language = $availableLanguages[$this->languageCode];
    }
    else {
      $language = $this->languageManager->getDefaultLanguage();
    }

    return array($methodId => $language);
  }

  /**
   * Set the fixed language.
   *
   * @param string $languageCode
   */
  public function setLanguageCode(string $languageCode): void {
    $this->languageCode = $languageCode;
  }

}
