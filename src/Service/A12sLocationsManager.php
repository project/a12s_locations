<?php

namespace Drupal\a12s_locations\Service;

use Drupal\a12s_locations\Exception\CacheUpdateException;
use Drupal\a12s_locations\Plugin\views\style\LocationsMap;
use Drupal\Core\Cache\Context\CacheContextsManager;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Theme\ThemeInitializationInterface;
use Drupal\Core\Theme\ThemeManagerInterface;
use Drupal\views\ViewExecutable;
use Drupal\views\Views;
use Twig\Environment;

/**
 * Class PriceManager
 */
class A12sLocationsManager {

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *    The language manager.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler service.
   * @param \Drupal\Core\Theme\ThemeManagerInterface $themeManager
   *   The theme manager service.
   * @param \Drupal\Core\Theme\ThemeInitializationInterface $themeInitialization
   *   The theme initialization service.
   * @param \Drupal\Core\Extension\ThemeHandlerInterface $themeHandler
   *   The theme handler service.
   * @param \Twig\Environment $twigService
   *   The Twig environment service.
   */
  public function __construct(
    protected LanguageManagerInterface $languageManager,
    protected ModuleHandlerInterface $moduleHandler,
    protected ThemeManagerInterface $themeManager,
    protected ThemeInitializationInterface $themeInitialization,
    protected ThemeHandlerInterface $themeHandler,
    protected Environment $twigService,
  ) {}

  public function prepareRenderContext(array $variant): void {
    $original = &drupal_static('a12s_locations_cache_update_original_context');
    $original = [];
    $contextTokens = array_keys($variant);

    foreach (CacheContextsManager::parseTokens($contextTokens) as $contextToken) {
      [$contextId, $parameter] = $contextToken;
      $context = \Drupal::getContainer()->get('cache_context.' . $contextId);
      $key = implode(':', $contextToken);
      $original[$key] = $context->getContext($parameter);

      // Force language.
      if ($contextId === 'languages') {
        static::forceLanguage($variant[$key], $parameter);
      }
    }

    \Drupal::moduleHandler()->invokeAll('a12s_locations_map_cache_prepare_render_context', [$variant]);

    // Force theme.
    $original['theme'] = $this->themeManager->getActiveTheme()->getName();
    $defaultTheme = $this->themeHandler->getDefault();

    if ($defaultTheme != $original['theme']) {
      $this->themeManager->setActiveTheme($this->themeInitialization->initTheme($defaultTheme));
    }

    // Disable TWIG debug mode.
    $original['twig_debug'] = $this->twigService->isDebug();
    if ($original['twig_debug']) {
      $this->twigService->disableDebug();
    }
  }

  public function restoreRenderContext(): void {
    $original = &drupal_static('a12s_locations_cache_update_original_context');
    $variant = $original;
    unset($variant['twig_debug'], $variant['theme']);

    // Restore original language.
    foreach (CacheContextsManager::parseTokens(array_keys($variant)) as $contextToken) {
      [$contextId, $parameter] = $contextToken;

      if ($contextId === 'languages') {
        static::forceLanguage($original[implode(':', $contextToken)], $parameter);
        break;
      }
    }

    \Drupal::moduleHandler()->invokeAll('a12s_locations_map_cache_restore_render_context', [$variant]);

    // Restore TWIG settings.
    if (!empty($original['twig_debug'])) {
      $this->twigService->enableDebug();
    }

    // Restore original theme.
    if (!empty($original['theme']) && $original['theme'] !== $this->themeManager->getActiveTheme()->getName()) {
      $this->themeManager->setActiveTheme($this->themeInitialization->initTheme($original['theme']));
    }

    unset($original['twig_debug'], $original['theme']);
  }

  /**
   * Execute the specified view with the given offset and limit.
   *
   * @param string $viewId
   *     The view ID.
   * @param string $displayId
   *     The view display ID.
   * @param int $offset
   *   The offset number for the view results.
   * @param int $limit
   *   The maximum number of results to fetch from the view.
   *
   * @return \Drupal\views\ViewExecutable|null
   *   The executed view object if it exists, otherwise null.
   *
   * @throws \Exception
   *   When there is an error on view execution.
   */
  public function executeView(string $viewId, string $displayId, int $offset = 0, int $limit = 10): ?ViewExecutable {
    if ($view = Views::getView($viewId)) {
      $view->initDisplay();
      $view->initQuery();
      //$view->query->setLimit($limit);
      //$view->query->setOffset($offset);
      //$view->setOffset($offset);
      //$view->setItemsPerPage($limit);
      //$view->setCurrentPage(round($offset / $limit));

      if (!$view->displayHandlers->has($displayId)) {
        throw new CacheUpdateException((string) $this->t('The view display is not valid.'));
      }

      $display = &$view->displayHandlers->get($displayId);
      // Don't store cache.
      $display->setOption('cache', ['type' => 'none']);
      $display->setOption('pager', [
        'type' => 'full',
        'options' => [
          'offset' => $offset,
          'items_per_page' => $limit,
        ],
      ]);

      $view->setDisplay($displayId);
      $view->preExecute();

      // Check style plugin.
      if (!$view->style_plugin instanceof LocationsMap) {
        throw new CacheUpdateException((string) $this->t('The style plugin is not valid.'));
      }

      $view->style_plugin->setCacheRebuilding(TRUE);

      $view->execute();
      return $view;
    }

    return NULL;
  }

  /**
   * Get the output of the specified view.
   *
   * @return array
   *   The output of the view if it has a result, otherwise an empty array.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Exception
   */
  public function getViewOutput(ViewExecutable $view): array {
    $originalTheme = $this->themeHandler->getDefault();

    if ($view->result) {
      $cache = Views::pluginManager('cache')->createInstance('none');
      $view->initStyle();

      if (!$view->style_plugin->usesRowPlugin()) {
        throw new CacheUpdateException((string) $this->t('The style plugin does not use rows.'));
      }

      if ($view->style_plugin->usesFields()) {
        foreach ($view->field as $id => $handler) {
          if (!empty($view->field[$id])) {
            $view->field[$id]->preRender($view->result);
          }
        }
      }

      $view->style_plugin->preRender($view->result);
      $this->moduleHandler->invokeAll('views_pre_render', [$view]);

      $function = $originalTheme . '_views_pre_render';
      if (function_exists($function)) {
        $function($view);
      }

      $view->display_handler->output = $view->display_handler->render();
      $this->moduleHandler->invokeAll('views_post_render', [$view, &$view->display_handler->output, $cache]);

      $function = $originalTheme . '_views_post_render';
      if (function_exists($function)) {
        $function($view, $view->display_handler->output, $cache);
      }

      return $view->display_handler->output;
    }

    return [];
  }

  /**
   * Set the negotiator and config language with the current language.
   *
   * @param string $languageId
   * @param string|NULL $languageType
   *
   * @return void
   */
  public static function forceLanguage(string $languageId, string $languageType = NULL): void {
    /** @var \Drupal\a12s_locations\Language\FixedLanguageNegotiator $fixedLanguageNegotiator */
    $fixedLanguageNegotiator = \Drupal::service('a12s_locations.fixed_language_negotiator');
    $languageManager = \Drupal::languageManager();
    $fixedLanguageNegotiator->setLanguageCode($languageId);
    $languageManager->setNegotiator($fixedLanguageNegotiator);
    $languageManager->reset($languageType);

    if ($language = $languageManager->getLanguage($languageId)) {
      $languageManager->setConfigOverrideLanguage($language);
    }
  }

}
