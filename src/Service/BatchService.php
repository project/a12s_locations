<?php

namespace Drupal\a12s_locations\Service;

use Drupal\a12s_locations\Plugin\views\style\LocationsMap;
use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Utility\Error;

/**
 * Custom service for the various batches callback.
 */
class BatchService {

  use DependencySerializationTrait;

  private const MAX_RESULTS_PER_QUERY = 50;

  /**
   * Define the batch for cache update.
   *
   * @param string $viewId
   * @param array $displays
   *
   * @return array
   */
  public static function getCacheUpdateBatchDefinition(string $viewId, array $displays): array {
    $batch = [
      'title' => t('Updating cached data for locations map...'),
      'operations' => [],
      'finished' => [self::class, 'finishedBatch'],
    ];

    $variants = [];

    foreach (\Drupal::languageManager()->getLanguages() as $language) {
      $variants[] = ['languages:' . LanguageInterface::TYPE_INTERFACE => $language->getId()];
    }

    $context = [
      'view_id' => $viewId,
      'displays' => $displays,
    ];

    \Drupal::moduleHandler()->alter('a12s_locations_map_cache_variants', $variants, $context);

    /** @var \Drupal\Core\Cache\Context\CacheContextsManager $cacheContextManager */
    $cacheContextManager = \Drupal::service('cache_contexts_manager');

    foreach ($displays as $displayId) {
      foreach ($variants as $variant) {
        if (!$cacheContextManager->assertValidTokens(array_keys($variant))) {
          continue;
        }

        $batch['operations'][] = [[self::class, 'buildList'], [$viewId, $displayId, $variant]];
        $batch['operations'][] = [[self::class, 'setCache'], [$viewId, $displayId, $variant]];
      }
    }

    return $batch;
  }

  /**
   * Build the list step by step.
   *
   * @param string $viewId
   *   The view ID.
   * @param string $displayId
   *   The view display ID.
   * @param string[] $variant
   *   An array whose keys are cache contexts and values a specific value for
   *   the given cache context.
   * @param array $context
   *   An array that contains information about the status of the batch, with
   *   the following keys:
   *   - sandbox: Use the $context['sandbox'] rather than $_SESSION to store the
   *     information needed to track information between successive calls to
   *     the current operation.
   *   - results: The array of results gathered so far by the batch processing.
   *     This array is highly useful for passing data between operations. After
   *     all operations have finished, these results may be referenced
   *     to display information to the end-user, such as how many total items
   *     were processed.
   *   - message: A text message displayed in the progress page.
   *   - finished: A float number between 0 and 1 informing the processing
   *     engine of the completion level for the operation.
   *     1 (or no value explicitly set) means the operation is finished and the
   *     batch processing can continue to the next operation.
   *     Batch API resets this to 1 each time the operation callback is called.
   */
  public static function buildList(string $viewId, string $displayId, array $variant, array &$context): void {
    /** @var \Drupal\a12s_locations\Service\A12sLocationsManager $a12sLocationsManager */
    $a12sLocationsManager = \Drupal::service('a12s_locations.manager');

    /** @var \Drupal\Core\Render\RendererInterface $rendererService */
    $renderer = \Drupal::service('renderer');

    /** @var \Drupal\Core\File\FileSystemInterface $fileSystem */
    $fileSystem = \Drupal::service('file_system');

    $sandbox = &$context['sandbox'];
    $results = &$context['results'];
    $context['message'] = t('Building cache chunks');
    $sandbox += [
      'limit' => self::MAX_RESULTS_PER_QUERY,
      'errors' => [],
    ];

    if (!isset($results['files'])) {
      // Data stored for all batch operations.
      $results['files'] = [];
      $results['errors'] = [];
      $results['total_items'] = 0;
    }

    if (!isset($sandbox['offset'])) {
      // Data stored only for current batch operations.
      $sandbox['offset'] = 0;
    }
    else {
      $sandbox['offset'] += $sandbox['limit'];
    }

    $a12sLocationsManager->prepareRenderContext($variant);

    try {
      $view = $a12sLocationsManager->executeView($viewId, $displayId, $sandbox['offset'], $sandbox['limit']);

      // @todo what if there are no results?
      if ($view && $view->result) {
        $count = count($view->result);

        if (empty($context['results']['total_items'])) {
          //$context['results']['total_items'] = $this->executeCountQuery($view) ?: $count;
          $context['results']['total_items'] = $view->total_rows ?: $count;
        }

        //$output = $view->render();
        $output = $a12sLocationsManager->getViewOutput($view);

        if (!empty($output['#rows'][0]['#rows'])) {
          $rowOutput = $output['#rows'][0]['#rows'];

          if (empty($context['results']['view_output'])) {
            // Preserve memory.
            $output['#rows'][0]['#rows'] = [];
            $context['results']['view_output'] = $output;
          }

          $html = array_map(function (array $row) use ($renderer) {
            return [
              '#markup' => $renderer->renderPlain($row),
            ];
          }, $rowOutput);

          if ($html) {
            $filePath = LocationsMap::getCacheFilePath($viewId, $displayId, $variant);
            $tempFile = $fileSystem->tempnam('temporary://', "a12s_locations_$filePath");
            $fileSystem->saveData(serialize($html), $tempFile, FileSystemInterface::EXISTS_REPLACE);
            $results['files'][] = $tempFile;
            $context['finished'] = min(round(($sandbox['offset'] + $count) / $context['results']['total_items'], 4), 1);
          }
        }
      }
    }
    catch (PluginException $e) {
      Error::logException(\Drupal::logger('a12s_locations'), $e);
    }
    catch (\Exception $e) {
      $sandbox['errors'] = $e->getMessage();
    }

    if (!empty($sandbox['errors'])) {
      $results['errors'][$viewId][$displayId] = $sandbox['errors'];
      $context['finished'] = 1;
    }

    if (!isset($context['results']['total_items'])) {
      $context['finished'] = 1;
    }

    if ($context['finished'] >= 1) {
      $context['message'] = t('Assembling cache chunks');
    }

    $a12sLocationsManager->restoreRenderContext();
  }

  /**
   * Store the list to the cache.
   *
   * @param string $viewId
   *    The view ID.
   * @param string $displayId
   *    The view display ID.
   * @param $context
   *   An array that contains information about the status of the batch.
   */
  public static function setCache(string $viewId, string $displayId, array $variant, array &$context): void {
    /** @var \Drupal\a12s_locations\A12sLocationsManager $a12sLocationsManager */
    $a12sLocationsManager = \Drupal::service('a12s_locations.manager');

    /** @var \Drupal\Core\Render\RendererInterface $rendererService */
    $renderer = \Drupal::service('renderer');

    /** @var \Drupal\Core\File\FileSystemInterface $fileSystem */
    $fileSystem = \Drupal::service('file_system');

    if (isset($context['results']['errors'][$viewId][$displayId])) {
      return;
    }

    if (empty($context['results']['view_output'])) {
      $context['results']['errors'][$viewId][$displayId] = t('There is no locations to cache.');
      return;
    }

    $a12sLocationsManager->prepareRenderContext($variant);
    $rows = [];

    foreach ($context['results']['files'] ?? [] as $file) {
      $rows = array_merge($rows, unserialize(file_get_contents($file)));
      $fileSystem->delete($file);
    }

    unset($context['results']['files']);

    if ($rows) {
      $output = $context['results']['view_output'];
      $output['#rows'][0]['#rows'] = $rows;

      $html = $renderer->renderPlain($output['#rows']);
      $filePath = LocationsMap::getCacheFilePath($viewId, $displayId, $variant);
      $directory = dirname($filePath);
      $tempFile = $fileSystem->tempnam('temporary://', "a12s_locations_$filePath");
      $fileSystem->saveData($html, $tempFile, FileSystemInterface::EXISTS_REPLACE);
      $fileSystem->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY);
      $fileSystem->move($tempFile, $filePath, FileSystemInterface::EXISTS_REPLACE);
      $fileSystem->delete($tempFile);
    }
    else {
      $context['results']['errors'][$viewId][$displayId] = t('An error occurred while creating the cached data. Was the page manually refreshed during the process?');
    }

    $a12sLocationsManager->restoreRenderContext();
  }

}
