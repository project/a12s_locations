<?php

namespace Drupal\a12s_locations\Commands;

use Drupal\a12s_locations\Plugin\views\style\LocationsMap;
use Drupal\a12s_locations\Service\BatchService;
use Drupal\views\DisplayPluginCollection;
use Drupal\views\Entity\View;
use Drupal\views\Views;
use Drush\Commands\DrushCommands;

/**
 * Class A12sLocationsCommands
 */
class A12sLocationsCommands extends DrushCommands {

  /**
   * Cache update for a view and display id given.
   *
   * @command a12s_locations:cache_update
   * @aliases alcu
   * @usage a12s_locations:cache_update
   */
  public function cacheUpdate(string $viewId, string $displayId = NULL): void {
    if ($view = View::load($viewId)) {
      $displays = [];

      if ($displayId) {
        $displays = [$displayId];
      }
      else {
        /** @var \Drupal\views\ViewExecutableFactory $viewFactory */
        $viewFactory = \Drupal::service('views.executable');
        $viewExecutable = $viewFactory->get($view);
        $displayHandlers = new DisplayPluginCollection($viewExecutable, Views::pluginManager('display'));

        /** @var \Drupal\views\Plugin\views\display\DisplayPluginBase $displayHandler */
        foreach ($displayHandlers as $id => $displayHandler) {
          $stylePlugin = $displayHandler->getPlugin('style');

          if ($stylePlugin instanceof LocationsMap && $stylePlugin->getCacheOption('enabled')) {
            $displays[] = $id;
          }
        }
      }

      if ($displays) {
        $batch = BatchService::getCacheUpdateBatchDefinition($viewId, $displays);
        batch_set($batch);
        drush_backend_batch_process();
      }
    }

  }

}
