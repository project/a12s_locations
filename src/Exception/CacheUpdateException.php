<?php

namespace Drupal\a12s_locations\Exception;

/**
 * Exception related to the cache update process for locations list.
 */
class CacheUpdateException extends \Exception {}
