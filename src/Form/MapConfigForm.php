<?php

declare(strict_types=1);

namespace Drupal\a12s_locations\Form;

use Drupal\a12s_locations\Plugin\MapProviderPluginManager;
use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\a12s_locations\Entity\MapConfig;
use Drupal\Core\Form\SubformState;
use Drupal\Core\Utility\Error;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Map configuration form.
 *
 * @property \Drupal\a12s_locations\Entity\MapConfigInterface $entity
 */
class MapConfigForm extends EntityForm {

  /**
   * Class constructor.
   *
   * @param MapProviderPluginManager $mapProviderPluginManager
   *   The map provider plugin manager.
   */
  public function __construct(protected MapProviderPluginManager $mapProviderPluginManager) {}

  /**
   * {@inheritDoc}
   *
   * @noinspection PhpParamsInspection
   */
  public static function create(ContainerInterface $container): static {
    return new static(
      $container->get('plugin.manager.a12s_locations_map_provider'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state): array {
    $form = parent::form($form, $form_state);

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->label(),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $this->entity->id(),
      '#machine_name' => [
        'exists' => [MapConfig::class, 'load'],
      ],
      '#disabled' => !$this->entity->isNew(),
    ];

    $pluginId = $this->entity->getPluginId();
    $pluginDefinitions = $this->mapProviderPluginManager->getDefinitions();

    $form['plugin_id'] = [
      '#type' => 'select',
      '#title' => $this->t('Map Provider'),
      '#default_value' => $pluginId,
      '#options' => array_map(fn($definition) => $definition['label'], $pluginDefinitions),
      '#empty_option' => $this->t('Select a provider'),
      '#required' => TRUE,
      '#disabled' => !$this->entity->isNew(),
      '#ajax' => [
        'callback' => '::updateMapProvider',
        'wrapper' => 'a12s-locations-map-provider-settings',
      ],
    ];

    if (!$this->entity->isNew() && !isset($pluginDefinitions[$pluginId])) {
      $this->messenger()->addWarning($this->t('The plugin %name does not exist. You should delete the current instance or enable the module which manage this plugin.', [
        '%name' => $pluginId,
      ]));
      $form['plugin']['#default_value'] = NULL;
      $form['plugin']['#required'] = FALSE;
    }

    $form['plugin_settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Map settings'),
      '#tree' => TRUE,
      '#prefix' => '<div id="a12s-locations-map-provider-settings">',
      '#suffix' => '</div>',
    ];

    /** @var \Drupal\a12s_locations\Plugin\MapProviderInterface $plugin */
    try {
      $pluginId = NestedArray::getValue($form_state->getUserInput(), ['plugin_id']) ?? $pluginId;

      if ($pluginId) {
        $pluginSettings = NestedArray::getValue($form_state->getUserInput(), ['plugin_settings']) ?? $this->entity->getPluginSettings();
        $plugin = $this->mapProviderPluginManager->createInstance($pluginId);
        $subForm = ['#parents' => ['plugin_settings', $pluginId]];
        $subformState = SubformState::createForSubform($subForm, $form, $form_state);
        $form['plugin_settings'][$pluginId] = $plugin->settingsForm($subForm, $subformState, $pluginSettings);
      }
    }
    catch (PluginException $e) {
      Error::logException($this->logger('a12s_locations'), $e);
    }

    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $this->entity->status(),
    ];

    return $form;
  }

  /**
   * Ajax callback to update the settings sub-form of the map provider.
   */
  public function updateMapProvider(array &$form, FormStateInterface $formState): array {
    return $form['plugin_settings'];
  }

  /**
   * {@inheritDoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    parent::validateForm($form, $form_state);

    if ($pluginId = $form_state->getValue('plugin_id')) {
      $subForm = &$form['plugin_settings'][$pluginId];

      if (isset($subForm)) {
        $plugin = $this->mapProviderPluginManager->createInstance($pluginId);
        $subformState = SubformState::createForSubform($subForm, $form, $form_state);
        $plugin->validateSettingsForm($subForm, $subformState);
      }
    }
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    if ($pluginId = $form_state->getValue('plugin_id')) {
      $plugin = $this->mapProviderPluginManager->createInstance($pluginId);
      $subformState = SubformState::createForSubform($form['plugin_settings'][$pluginId], $form, $form_state);
      $plugin->submitSettingsForm($form['plugin_settings'][$pluginId], $subformState);
    }

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): int {
    $result = parent::save($form, $form_state);
    $messageArgs = ['%label' => $this->entity->label()];
    $this->messenger()->addStatus(
      match($result) {
        \SAVED_NEW => $this->t('Created new map configuration %label.', $messageArgs),
        \SAVED_UPDATED => $this->t('Updated map configuration %label.', $messageArgs),
      }
    );
    $form_state->setRedirectUrl($this->entity->toUrl('collection'));
    return $result;
  }

}
