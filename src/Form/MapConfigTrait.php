<?php

declare(strict_types=1);

namespace Drupal\a12s_locations\Form;

use Drupal\a12s_locations\Entity\MapConfig;
use Drupal\a12s_locations\Plugin\MapProviderInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

Trait MapConfigTrait {

  use StringTranslationTrait;

  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Gets the MapProviderInterface plugin instance from a map configuration ID.
   *
   * @param string|null $mapConfigId
   *   The ID of the map configuration.
   *
   * @return MapProviderInterface|null
   *   The MapProviderInterface plugin instance associated with the map
   *   configuration ID, or NULL if not found.
   */
  public function getPluginFromMapConfigId(?string $mapConfigId): ?MapProviderInterface {
    return !empty($mapConfigId) ? MapConfig::load($mapConfigId)?->getPlugin() : NULL;
  }

  /**
   * Get the map provider form element.
   *
   * @param string $inputName
   *   The name of the input element (default: 'map_configuration').
   *
   * @return array
   *   The form API element.
   */
  public function mapProviderFormElement(?string $defaultValue, string $inputName = 'map_configuration'): array {
    $MapConfigStorage = $this->entityTypeManager->getStorage('a12s_locations_map_config');
    $MapConfigs = $MapConfigStorage->loadByProperties(['status' => TRUE]);

    if (!$MapConfigs) {
      $element[$inputName . '_warning'] = [
        '#theme' => 'status_messages',
        '#message_list' => [
          'warning' => [
            $this->t('There are no active map configurations. Please create at least one configuration to be able to execute the view.'),
          ],
        ],
        '#status_headings' => [
          'warning' => $this->t('Warning message'),
        ],
      ];

      $element[$inputName] = [
        '#type' => 'value',
        '#value' => '',
      ];
    }
    else {
      $element[$inputName] = [
        '#type' => 'select',
        '#title' => $this->t('Map configuration'),
        '#options' => array_map(fn($config) => $config->label(), $MapConfigs),
        '#default_value' => $defaultValue,
        '#required' => TRUE,
      ];
    }

    return $element;
  }

}
