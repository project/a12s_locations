<?php

declare(strict_types = 1);

namespace Drupal\a12s_locations\Form;

use Drupal\a12s_locations\Plugin\views\style\LocationsMap;
use Drupal\a12s_locations\Service\BatchService;
use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Entity\EntityDeleteFormTrait;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\views\DisplayPluginCollection;
use Drupal\views\ViewExecutableFactory;
use Drupal\views\Views;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Update the cached data of a view.
 *
 * @property \Drupal\views\ViewEntityInterface entity
 * @method \Drupal\views\ViewEntityInterface getEntity()
 */
class CacheUpdateConfirmForm extends EntityConfirmFormBase {

  use EntityDeleteFormTrait;

  /**
   * Class constructor.
   *
   * @param \Drupal\views\ViewExecutableFactory $viewFactory
   *   The view executable factory.
   */
  public function __construct(
    protected ViewExecutableFactory $viewFactory,
  ) {}

  /**
   * {@inheritdoc}
   *
   * @noinspection PhpParamsInspection
   */
  public static function create(ContainerInterface $container): static {
    return new static(
      $container->get('views.executable'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $view = $this->viewFactory->get($this->getEntity());
    $displayHandlers = new DisplayPluginCollection($view, Views::pluginManager('display'));
    $options = [];

    /** @var \Drupal\views\Plugin\views\display\DisplayPluginBase $displayHandler */
    foreach ($displayHandlers as $id => $displayHandler) {
      $stylePlugin = $displayHandler->getPlugin('style');

      if ($stylePlugin instanceof LocationsMap && $stylePlugin->getCacheOption('enabled')) {
        $options[$id] = $displayHandler->display['display_title'];
      }
    }

    if (!$options) {
      throw new NotFoundHttpException();
    }

    $form = parent::buildForm($form, $form_state);

    $form['displays'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Displays'),
      '#description' => $this->t('Select the displays for which you want to delete the cached data. If none are selected, the cache will be rebuilt for all displays.'),
      '#options' => $options,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion(): TranslatableMarkup {
    return $this->t('Are you sure you want to update the cached data of the @entity-type %label?', [
      '@entity-type' => $this->getEntity()->getEntityType()->getSingularLabel(),
      '%label' => $this->getEntity()->label() ?? $this->getEntity()->id(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText(): TranslatableMarkup {
    return $this->t('Update cache');
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription(): TranslatableMarkup {
    return $this->t('The cached data for the list of points of sales are about to be deleted and recalculated. This action cannot be undone.');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $displays = $form_state->getValue('displays', []);
    $displays = array_keys(array_filter($displays) ?: $displays);
    $batch = BatchService::getCacheUpdateBatchDefinition($this->getEntity()->id(), $displays);
    batch_set($batch);
  }

}
