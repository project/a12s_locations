<?php

namespace Drupal\a12s_locations\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Location map settings form.
 */
class LocationsMapSettingsForm extends ConfigFormBase {

  const A12S_LOCATIONS_SETTINGS = 'a12s_locations.settings';

  /**
   * @inheritDoc
   */
  protected function getEditableConfigNames(): array {
    return [self::A12S_LOCATIONS_SETTINGS];
  }

  /**
   * @inheritDoc
   */
  public function getFormId(): string {
    return 'a12s_locations_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildForm($form, $form_state);
    $config = $this->config(self::A12S_LOCATIONS_SETTINGS);

    // Google Map API key.
    $form['google_map_api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Google Map API key'),
      '#default_value' => $config->get('google_map_api_key'),
    ];

    // Baidu Map API key.
    $form['baidu_map_api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Baidu Map API key'),
      '#default_value' => $config->get('baidu_map_api_key'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $config = $this->config(self::A12S_LOCATIONS_SETTINGS);
    $config->set('google_map_api_key', $form_state->getValue('google_map_api_key'));
    $config->set('baidu_map_api_key', $form_state->getValue('baidu_map_api_key'));
    $config->save();

    parent::submitForm($form, $form_state);
  }

}
