<?php

namespace Drupal\a12s_locations\Entity;

use Drupal\a12s_locations\Plugin\MapProviderInterface;
use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a map configuration.
 */
interface MapConfigInterface extends ConfigEntityInterface {

  /**
   * Whether the instance has a defined plugin.
   *
   * @return bool
   *   TRUE if the plugin exists.
   */
  public function hasPlugin(): bool;

  /**
   * Get the plugin ID.
   *
   * @return string|null
   *   The plugin ID for this configuration.
   */
  public function getPluginId(): ?string;

  /**
   * Returns the plugin instance.
   *
   * @return \Drupal\a12s_locations\Plugin\MapProviderInterface|null
   *   The plugin instance for this configuration.
   */
  public function getPlugin(): MapProviderInterface|null;

  /**
   * Retrieves the settings of the plugin.
   *
   * @return array
   *   The settings of the plugin.
   */
  public function getPluginSettings(): array;

}
