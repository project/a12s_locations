<?php

declare(strict_types=1);

namespace Drupal\a12s_locations\Entity;

use Drupal\a12s_locations\Plugin\MapProviderInterface;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Entity\EntityWithPluginCollectionInterface;
use Drupal\Core\Plugin\DefaultSingleLazyPluginCollection;

/**
 * Defines the map configuration entity type.
 *
 * @ConfigEntityType(
 *   id = "a12s_locations_map_config",
 *   label = @Translation("Map configuration"),
 *   label_collection = @Translation("Map configurations"),
 *   label_singular = @Translation("map configuration"),
 *   label_plural = @Translation("map configurations"),
 *   label_count = @PluralTranslation(
 *     singular = "@count map configuration",
 *     plural = "@count map configurations",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\a12s_locations\Entity\MapConfigListBuilder",
 *     "form" = {
 *       "add" = "Drupal\a12s_locations\Form\MapConfigForm",
 *       "edit" = "Drupal\a12s_locations\Form\MapConfigForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *   },
 *   config_prefix = "map_config",
 *   admin_permission = "administer a12s_locations_map_config",
 *   links = {
 *     "collection" = "/admin/config/services/a12s-locations/map-config",
 *     "add-form" = "/admin/config/services/a12s-locations/map-config/add",
 *     "edit-form" = "/admin/config/services/a12s-locations/map-config/{a12s_locations_map_config}",
 *     "delete-form" = "/admin/config/services/a12s-locations/map-config/{a12s_locations_map_config}/delete",
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "plugin_id",
 *     "plugin_settings",
 *   },
 * )
 */
class MapConfig extends ConfigEntityBase implements MapConfigInterface, EntityWithPluginCollectionInterface {

  /**
   * The identifier.
   */
  protected string $id;

  /**
   * The label.
   */
  protected string $label;

  /**
   * The "map provider" plugin ID.
   */
  protected string $plugin_id;

  /**
   * The "map provider" plugin settings.
   */
  protected array $plugin_settings;

  /**
   * {@inheritDoc}
   */
  public function hasPlugin(): bool {
    return !empty($this->plugin_id);
  }

  /**
   * {@inheritDoc}
   */
  public function getPluginId(): ?string {
    return $this->plugin_id ?? NULL;
  }

  /**
   * {@inheritDoc}
   */
  public function getPlugin(): ?MapProviderInterface {
    if ($this->hasPlugin()) {
      return $this->getPluginCollection()->get($this->plugin_id);
    }

    return NULL;
  }

  public function getPluginSettings(): array {
    if ($this->hasPlugin() && !empty($this->plugin_settings[$this->plugin_id])) {
      return $this->plugin_settings[$this->plugin_id];
    }

    return [];
  }

  /**
   * {@inheritDoc}
   */
  public function getPluginCollections(): array {
    if ($this->hasPlugin()) {
      return ['a12s_locations_map_provider' => $this->getPluginCollection()];
    }

    return [];
  }

  /**
   * Encapsulates the creation of the LazyPluginCollection.
   *
   * @return \Drupal\Core\Plugin\DefaultSingleLazyPluginCollection
   *   The plugin collection.
   */
  protected function getPluginCollection(): DefaultSingleLazyPluginCollection {
    if (!isset($this->pluginCollection)) {
      $this->pluginCollection = new DefaultSingleLazyPluginCollection(\Drupal::service('plugin.manager.a12s_locations_map_provider'), $this->plugin_id, $this->getPluginSettings());
    }
    return $this->pluginCollection;
  }

}
