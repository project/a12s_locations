<?php

namespace Drupal\a12s_locations\Ajax;

use Drupal\Core\Ajax\CommandInterface;

/**
 * AJAX command for updating the enabled locations on map.
 *
 * This command is implemented by Drupal.AjaxCommands.prototype.a12sLocationsMapUpdate().
 *
 * @ingroup ajax
 */
class LocationsMapUpdateCommand implements CommandInterface {

  /**
   * A CSS selector string.
   *
   * @var string
   */
  protected string $selector;

  /**
   * The IDs of the enabled locations.
   *
   * @var string[]
   */
  protected array $ids;

  /**
   * Optional data.
   *
   * @var array
   */
  protected array $data;

  /**
   * Constructs an LocationsMapUpdateCommand object.
   *
   * @param string $selector
   *   The CSS selector of the root element.
   * @param array $ids
   *   The list of enabled location IDs.
   * @param array $data
   *   Optional data.
   */
  public function __construct(string $selector, array $ids, array $data = []) {
    $this->selector = $selector;
    $this->ids = $ids;
    $this->data = $data;
  }

  /**
   * {@inheritdoc}
   */
  public function render(): array {
    return [
      'command' => 'a12sLocationsMapUpdate',
      'selector' => $this->selector,
      'enabledLocations' => $this->ids,
      'data' => $this->data,
    ];
  }

}
