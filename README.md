# A12s Locations

The A21s Location module helps to display a location map with markers and groups
of markers.

It uses a plugin system to integrate with map providers and defines currently 2
plugins for Google Map and Baidu Map.

## Features

It provides a field formatter for Geofield which displays a simple map with a 
marker for each point coordinates.

It also provide a view style which displays a list of locations on the map, wit
h support of info window, fit to bounds, auto-centering on marker when the
location is clicked on the list...

## Post-Installation

After installing the module, you need to define the API key for the map
provider(s) you want to use.

Then you can create some map configurations which are necessary to use the field
formatter or view style plugin. A map configuration is simply a list of options to customize the map (for example map style).

## Additional Requirements

This module relies on third-party script, like Google Map API or Baidu Map API.
You need to generate an API key for those providers before being able to use their libraries.
