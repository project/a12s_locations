import {
  a12sLocationsMapUpdateCommand,
  BaseMapsSettings,
  DrupalMapInterface,
  LocationMapsSettings,
  SimpleMapsSettings
} from "./types";

declare global {

  namespace drupal {

    namespace Core {

      export interface IBehaviors {

        a12sLocationsBaiduMap?: IBehavior & {
          initialized: boolean
        };

      }
    }

    interface IDrupalSettings {

      a12sLocations?: {

        locationMaps?: {
          [key: string]: LocationMapsSettings
        }

        simpleMaps?: {
          [key: string]: SimpleMapsSettings
        }

        baiduMapKey?: string

        [key: string]: string | {
          [key: string]: BaseMapsSettings
        }
      };

    }

    namespace Core {

      interface IAjaxCommands {

        a12sLocationsMapUpdate: a12sLocationsMapUpdateCommand;

      }

    }

  }

  interface Window {

    DrupalMap: DrupalMapInterface

    BMAP_PROTOCOL?: string;

    BMap_loadScriptTime?: number;

  }

}

export {};
