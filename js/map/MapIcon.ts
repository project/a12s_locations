import {drupalMap} from "../types";

export default class MapIcon {

  static readonly SIZE_HEIGHT_DEFAULT = 32;
  static readonly SIZE_WIDTH_DEFAULT = 32;

  readonly path: string;
  readonly size: drupalMap.Icon.Size;
  readonly anchor: drupalMap.Icon.Position;
  readonly origin: drupalMap.Icon.Position;

  /**
   * Constructs a new instance of the MapIcon class.
   *
   * @param {string} urlOrString - The URL or base64 encoded string representing
   *   the icon image, e.g. "data:image/svg+xml;base64,...".
   * @param {drupalMap.Icon.Options} [options] - Optional settings for the map icon.
   */
  constructor(urlOrString: string, options?: drupalMap.Icon.Options) {
    this.path = urlOrString;

    if (!options?.size) {
      // For SVG, we can try to find out the dimensions. But for remote images,
      // this is not possible as we cannot use a synchronous method here.
      const match = urlOrString.match(/^data:image\/svg(?:\+xml)?;base64,/);

      if (match) {
        try {
          const container = document.createElement('div');
          container.innerHTML = atob(urlOrString.replace(match[0], ''));
          const svgElement = container.querySelector('svg');
          document.body.insertAdjacentElement('beforeend', svgElement);
          const bounds = svgElement.getBBox();
          svgElement.remove();
          container.remove();

          this.size = {
            height: Math.ceil(bounds.height),
            width: Math.ceil(bounds.width)
          };

          // @todo should we use the bounds.x / bounds.y as offset?
        }
        catch (e) {
          // Bad encoded SVG?
          console.warn(e.message);
        }
      }
    }

    if (!this.size) {
      this.size = options?.size || {
        height: window.DrupalMap.MapIcon.SIZE_HEIGHT_DEFAULT,
        width: window.DrupalMap.MapIcon.SIZE_WIDTH_DEFAULT
      };
    }

    this.anchor = options?.anchor || undefined;
    this.origin = options?.origin || undefined;
  }

  public static buildFromIconData(data: any): MapIcon|undefined {
    if (typeof data === 'string') {
      return new window.DrupalMap.MapIcon(data);
    }

    if (typeof data === 'object' && data.hasOwnProperty('url')) {
      return new window.DrupalMap.MapIcon(data.url, data.options || {});
    }
  }

}
