import BaseMap from "./providers/BaseMap";
import {DrupalMapInterface, SimpleMapInterface} from "../types";
import Location from "./Location";
import MapPoint from "./MapPoint";
import MapIcon from "./MapIcon";
import Subscriber from "./Subscriber";
import SimpleMap from "./modules/simple/SimpleMap";

export default {

  Subscriber: Subscriber,

  Map: BaseMap,

  MapPoint: MapPoint,

  Location: Location,

  MapIcon: MapIcon,

  instances: new Map(),

  providers: {},

  modules: {
    SimpleMap: SimpleMap,
  },

  getMapRootElement(settings: any): HTMLElement|undefined {
    if (typeof settings.selector !== "undefined") {
      const mapRoot = document.querySelector(settings.selector);

      if (mapRoot instanceof HTMLElement) {
        return mapRoot;
      }
    }
  },

  async getMapModuleAsync(mapRoot: HTMLElement): Promise<SimpleMapInterface> {
    return new Promise((resolve) => {
      let map = this.instances.get(mapRoot);

      if (map) {
        resolve(map);
      }

      mapRoot.addEventListener('createdA12sDrupalMap', (event: CustomEvent) => {
        resolve(event.detail.map);
      }, { once: true });
    });

  }

} as DrupalMapInterface;
