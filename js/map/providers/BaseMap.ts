import {
  DrupalMapBaseMapSettings,
  ExtendedMarker,
  LocationInterface,
  LocationMarkerOptions,
  MapIconInterface,
  MapPointInterface,
  SubscriberInterface
} from "../../types";

/**
 * Handle the initialization of a Map using Drupal settings.
 *
 * @class
 */
class Initializer {

  initCallbacks: SubscriberInterface[] = [];

  add(subscriber: SubscriberInterface) {
    this.initCallbacks.push(subscriber);
  }

  /**
   * Map init callback.
   *
   * This method may be called by different ways, depending on the map provider.
   *
   * For example with Google, this method defined as a callback when the GMap
   * script is loaded:
   * https://maps.googleapis.com/maps/api/js?key=...&callback=drupalMap.google.init:
   *
   * With Baidu, there is no such load callback, so we handle this by ourselves
   * to have a similar behavior.
   */
  init = (context?: Document|DocumentFragment|Element) => {
    this.initCallbacks.forEach((subscriber) => {
      subscriber.initAllMaps(context);
    });
  }

}

const initializer = new Initializer();

/**
 * A class representing an abstract map.
 *
 * This class is supposed to be extended by specialized code for a specific map
 * provider (Google, Baidu...).
 *
 * @class
 * @property {HTMLElement} element - The HTML element containing the map.
 * @property {any} map - The Map object (this depends on the map provider).
 */
export default abstract class BaseMap {

  element: HTMLElement;

  settings: DrupalMapBaseMapSettings;

  map: any;

  protected infoWindow: any;

  protected markerCluster: any = null;

  /**
   * Constructor for creating a Map object.
   *
   * @constructor
   *
   * @param {HTMLElement} element - The HTML element where the map should be displayed.
   * @param {DrupalMapBaseMapSettings|null} settings - The map options.
   *
   * @throws {Error} - Throws an error if an attempt is made to instantiate Map class directly.
   */
  protected constructor(element: HTMLElement, settings?: DrupalMapBaseMapSettings) {
    this.element = element;
    this.settings = settings || {};
    this.infoWindow = null;
    this.map = this.initMap(element);
  }

  /**
   * Checks if the JavaScript libraries for the provider is ready.
   *
   * Child classes should override this method.
   *
   * @return {boolean} Returns true if the provider is ready, false otherwise.
   */
  public static providerIsReady(): boolean {
    return false;
  }

  protected abstract initMap(element: HTMLElement): any;

  public abstract setCenter(point: MapPointInterface): void;

  public abstract getZoom(): number;

  public abstract setZoom(zoom: number): void;

  public abstract addMapEventListener(event: string, callback: (...args: any[]) => void): void;

  public abstract setBoundsFromMarkers(markers: any[]): void;

  public abstract setBoundsFromMapPoints(points: MapPointInterface[]): void;

  public abstract addMarker(location: LocationInterface|MapPointInterface, options?: Partial<LocationMarkerOptions>, enabled?: boolean): any;

  public abstract enableMarker(location: LocationInterface): void;

  public abstract disableMarker(location: LocationInterface): void;

  public abstract setMarkerIcon(marker: any & ExtendedMarker, icon: MapIconInterface): void;

  /**
   * Adds an event listener to a marker.
   *
   * The child classes should ensure that "this" inside the callback points to
   * the target marker.
   *
   * @param {any} marker - The marker object to add the event listener to.
   * @param {string} event - The event name to listen for.
   * @param {function} callback - The event handler function to be called when the event occurs.
   */
  public abstract addMarkerEventListener(marker: any, event: string, callback: (...args: any[]) => void): void;

  public abstract isMarkerVisible(marker: any): boolean;

  /**
   * Set the content of the info window.
   *
   * Child classes may override this method if their map provider supports info
   * window.
   */
  public setInfoWindowContent(content: string | HTMLElement): void {}

  /**
   * Opens the info window.
   *
   * Child classes may override this method if their map provider supports info
   * window.
   */
  public openInfoWindow(anchor: any | MapPointInterface | ExtendedMarker): void {}

  /**
   * Closes the info window.
   *
   * Child classes may override this method if their map provider supports info
   * window.
   */
  public closeInfoWindow(): void {}

  /**
   * Adds an event listener to the info window.
   *
   * Child classes may override this method if their map provider supports info
   * window.
   */
  public addInfoWindowEventListener(event: string, callback: (...args: any[]) => void): void {}

  /**
   * Whether the map provider supports marker clusterer.
   *
   * Child classes may override this method if their map provider supports info
   * window.
   */
  public supportMarkerClusterer(): boolean {
    return false;
  }

  public initMarkerClusterer(): void {}

  public addMarkersToClusterer(markers: any[]): void {}

  public clearMarkersFromClusterer(): void {}

  public static subscribe(subscriber: SubscriberInterface) {
    initializer.add(subscriber);
  }

  public static init(context?: Document|DocumentFragment|Element) {
    initializer.init(context);
  }

  public static async loadScriptAsync(src: string) {
    const script = document.createElement('script');
    script.src = src;
    script.type = 'text/javascript';

    return new Promise((resolve, reject) => {
      script.onload = () => {
        resolve(true);
      };

      script.onerror = () => {
        reject(`Loading script "${src}" failed.`);
      }

      document.getElementsByTagName('head')[0].appendChild(script);
    });
  }

}
