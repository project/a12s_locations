import {
  ExtendedMarker,
  LocationInterface,
  LocationMarkerOptions,
  MapIconInterface,
  MapPointInterface
} from "../../../types";
import {
  MarkerClusterer,
  MarkerClustererOptions
} from "@googlemaps/markerclusterer/dist/markerclusterer";

type GoogleMapExtendedMarker = google.maps.Marker & ExtendedMarker;

/**
 * Represents a BaiduMap object.
 */
export default class GoogleMap extends window.DrupalMap.Map {

  map: google.maps.Map;

  infoWindow: google.maps.InfoWindow;

  markerCluster: MarkerClusterer;

  boundsBeingUpdatedFromMarkers: google.maps.LatLngBounds|null = null;

  public static providerIsReady(): boolean {
    return !!(window.google?.maps?.hasOwnProperty('Map') && typeof window.google.maps.Map === 'function');
  }

  protected initMap(element: HTMLElement): any {
    const centerLat = this.settings.center?.lat || 46.3489558;
    const centerLng = this.settings.center?.lng || 3.7962814;

    const mapOptions = {
      center: new google.maps.LatLng(centerLat, centerLng),
      zoom: this.settings.initialZoom || 6,
      mapTypeId: this.settings.provider.maxZoom || google.maps.MapTypeId.ROADMAP,
      mapTypeControl: false,
      scrollwheel: false,
      minZoom:  this.settings.minZoom || null,
      maxZoom:  this.settings.maxZoom || null,
    };

    const map = new google.maps.Map(element, mapOptions);
    const styleJson = this.settings?.style;

    if (styleJson) {
      const style = new google.maps.StyledMapType(styleJson, {
        // @todo pass provider map preset?
        name: 'Google map',
        alt: 'Google map',
      });
      map.mapTypes.set('map_style', style);
      map.setMapTypeId('map_style');
    }

    this.infoWindow = new google.maps.InfoWindow(this.settings.infoWindowOptions || null);

    // Ensure that the zoom level is not too big when we try to display all the
    // markers on the map.
    //map.addListener('bounds_changed', () => {
    //  if (this.boundsBeingUpdatedFromMarkers && map.getZoom() < 2) {
    //    map.setZoom(2);
    //    map.setCenter(this.boundsBeingUpdatedFromMarkers.getCenter());
    //    this.boundsBeingUpdatedFromMarkers = null;
    //  }
    //});

    return map;
  };

  public setCenter(point: MapPointInterface): void {
    this.map.setCenter(new google.maps.LatLng(point.lat, point.lng));
  }

  public getZoom(): number {
    return this.map.getZoom();
  }

  public setZoom(zoom: number): void {
    this.map.setZoom(zoom);
  }

  public addMapEventListener(event: string, callback: (...args: any[]) => void): void {
    this.map.addListener(event, callback);
  }

  public setBoundsFromMarkers(markers: GoogleMapExtendedMarker[]): void {
    if (markers.length) {
      const bounds = new google.maps.LatLngBounds();

      markers.forEach((marker) => {
        bounds.extend(marker.getPosition());
      });

      this.boundsBeingUpdatedFromMarkers = bounds;
      this.map.fitBounds(bounds);
    }
  }

  public setBoundsFromMapPoints(points: MapPointInterface[]): void {
    if (points.length) {
      const bounds = new google.maps.LatLngBounds();

      points.forEach((point) => {
        const latLng: google.maps.LatLng = new google.maps.LatLng(point.lat, point.lng);
        bounds.extend(latLng);
      });

      this.boundsBeingUpdatedFromMarkers = bounds;
      this.map.fitBounds(bounds);
    }
  }

  public addMarker(location: LocationInterface|MapPointInterface, options: Partial<LocationMarkerOptions>, enabled: boolean = true): GoogleMapExtendedMarker {
    // @todo factorize this between providers.
    const isLocation = 'mapPoint' in location;
    let lat: number;
    let lng: number;

    if (isLocation) {
      lat = location.mapPoint.lat;
      lng = location.mapPoint.lng;
    }
    else {
      lat = location.lat;
      lng = location.lng;
    }

    const marker = new google.maps.Marker({
      position: new google.maps.LatLng(lat, lng),
      map: enabled ? this.map : null
    }) as GoogleMapExtendedMarker;

    if (isLocation) {
      marker.a12sLocation = location;
    }

    return marker;
  }

  public enableMarker(location: LocationInterface): void {
    location.marker.setMap(this.map);
  }

  public disableMarker(location: LocationInterface): void {
    location.marker.setMap(null);
  }

  public setMarkerIcon(marker: GoogleMapExtendedMarker, mapIcon: MapIconInterface): void {
    try {
      const icon = {
        url: mapIcon.path
      } as google.maps.Icon;

      // We do not force the size, as Google make usually good calculation for this.
      if (mapIcon.size) {
        icon.size = new google.maps.Size(mapIcon.size.width, mapIcon.size.height);
      }

      if (mapIcon.anchor) {
        icon.anchor = new google.maps.Point(mapIcon.anchor.x, mapIcon.anchor.y);
      }

      if (mapIcon.origin) {
        icon.origin = new google.maps.Point(mapIcon.origin.x, mapIcon.origin.y);
      }

      marker.setIcon(icon);
    }
    catch (e) {
      console.warn(e.message);
    }
  }

  public addMarkerEventListener(marker: GoogleMapExtendedMarker, event: string, callback: (...args: any[]) => void): void {
    marker.addListener(event, callback);
  }

  public isMarkerVisible(marker: any): boolean {
    return this.map.getBounds().contains(marker.getPosition());
  }

  public setInfoWindowContent(content: string | HTMLElement): void {
    this.infoWindow.setContent(content);
  }

  public openInfoWindow(anchor: google.maps.Point | google.maps.Marker | MapPointInterface): void {
    const options: google.maps.InfoWindowOpenOptions = {
      map: this.map
    };

    if (anchor instanceof google.maps.MVCObject) {
      options.anchor = anchor;
    }
    else if ('lng' in anchor && 'lat' in anchor) {
      options.anchor = new google.maps.Marker({
        position: new google.maps.LatLng(anchor.lat, anchor.lng),
        map: this.map
      });
    }

    this.infoWindow.open(options);
  }

  public closeInfoWindow(): void {
    this.infoWindow.close();
  }

  public addInfoWindowEventListener(event: string, callback: (...args: any[]) => void): void {
    // @todo convert "close" to "closeclick"?
    this.infoWindow.addListener(event, callback);
  }

  public supportMarkerClusterer(): boolean {
    return typeof window.DrupalMap.providers?.google?.MarkerClusterer === 'function';
  }

  public initMarkerClusterer(): void {
    if (this.supportMarkerClusterer()) {
      const clusterOptions: Partial<MarkerClustererOptions> = this.settings.clusterOptions || {};
      clusterOptions.map = this.map;
      this.markerCluster = new window.DrupalMap.providers.google.MarkerClusterer(clusterOptions);
    }
  }

  public addMarkersToClusterer(markers: any[]): void {
    if (this.markerCluster) {
      this.markerCluster.addMarkers(markers);
    }
  }

  public clearMarkersFromClusterer(): void {
    if (this.markerCluster) {
      this.markerCluster.clearMarkers();
    }
  }

}
