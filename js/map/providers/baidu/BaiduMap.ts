import {
  ExtendedMarker,
  LocationInterface,
  LocationMarkerOptions, MapIconInterface,
  MapPointInterface
} from "../../../types";

type BaiduMapExtendedMarker = BMap.Marker & ExtendedMarker;

/**
 * Represents a BaiduMap object.
 */
export default class BaiduMap extends window.DrupalMap.Map {

  readonly INFO_WINDOW_OPTIONS_DEFAULT = {
    width : 250,
    height: 100,
    title : ''
  };

  map: BMap.Map;

  infoWindow: BMap.InfoWindow;

  public static providerIsReady(): boolean {
    return !!(window.BMap?.hasOwnProperty('Map') && typeof window.BMap.Map === 'function');
  }

  protected initMap(element: HTMLElement): any {
    const map = new BMap.Map(element, {
      mapType: BMAP_NORMAL_MAP
    });

    const styleJson = this.settings?.style;

    if (styleJson) {
      map.setMapStyle(styleJson);
    }

    // Default point: center of China.
    const centerLat = this.settings.center?.lat || 35.844694;
    const centerLng = this.settings.center?.lng || 103.452083;
    let initPoint = new BMap.Point(centerLng, centerLat);
    map.centerAndZoom(initPoint, this.settings.initialZoom || 5);

    // @todo use InfoBox instead?
    //   https://api.map.baidu.com/library/InfoBox/1.2/docs/symbols/BMapLib.InfoBox.html
    const infoWindowOptions = Object.assign({}, this.INFO_WINDOW_OPTIONS_DEFAULT, this.settings.infoWindowOptions || {})
    this.infoWindow = new BMap.InfoWindow('', infoWindowOptions);

    // @todo use settings.
    map.enableContinuousZoom();
    map.enableDragging();
    map.enableScrollWheelZoom();
    map.enableDoubleClickZoom();
    map.enableKeyboard();

    map.addControl(new BMap.NavigationControl({
      anchor: BMAP_ANCHOR_TOP_LEFT,
      type: BMAP_NAVIGATION_CONTROL_ZOOM
    }));

    map.addControl(new BMap.OverviewMapControl({
      anchor: BMAP_ANCHOR_BOTTOM_RIGHT,
      isOpen: true
    }));

    map.addControl(new BMap.ScaleControl({
      anchor: BMAP_ANCHOR_BOTTOM_RIGHT
    }));

    return map;
  };

  public setCenter(point: MapPointInterface): void {
    this.map.setCenter(new BMap.Point(point.lng, point.lat));
  }

  public getZoom(): number {
    return this.map.getZoom();
  }

  public setZoom(zoom: number): void {
    this.map.setZoom(zoom);
  }

  public addMapEventListener(event: string, callback: (...args: any[]) => void): void {
    this.map.addEventListener(event, callback);
  }

  public setBoundsFromMarkers(markers: BaiduMapExtendedMarker[]): void {
    const points: BMap.Point[] = [];

    markers.forEach((marker) => {
      const position = marker.getPosition();
      points.push(new BMap.Point(position.lng, position.lat));
    });

    this.map.setViewport(points);
  }

  public setBoundsFromMapPoints(points: MapPointInterface[]): void {
    const bMapPoints: BMap.Point[] = [];

    points.forEach((point) => {
      bMapPoints.push(new BMap.Point(point.lng, point.lat));
    });

    this.map.setViewport(bMapPoints);
  }

  public addMarker(location: LocationInterface|MapPointInterface, options: Partial<LocationMarkerOptions>, enabled: boolean = true): BaiduMapExtendedMarker {
    const isLocation = 'mapPoint' in location;
    let lat: number;
    let lng: number;

    if (isLocation) {
      lat = location.mapPoint.lat;
      lng = location.mapPoint.lng;
    }
    else {
      lat = location.lat;
      lng = location.lng;
    }
    const point = new BMap.Point(lng, lat);
    const marker = new BMap.Marker(point) as BaiduMapExtendedMarker;

    if (isLocation) {
      marker.a12sLocation = location;
    }

    if (enabled) {
      this.map.addOverlay(marker);
    }

    return marker;
  }

  public enableMarker(location: LocationInterface): void {
    this.map.removeOverlay(location.marker);
  }

  public disableMarker(location: LocationInterface): void {
    this.map.addOverlay(location.marker);
  }

  public setMarkerIcon(marker: BaiduMapExtendedMarker, mapIcon: MapIconInterface): void {
    try {
      const size = new BMap.Size(mapIcon.size?.width || window.DrupalMap.MapIcon.SIZE_WIDTH_DEFAULT, mapIcon.size?.height || window.DrupalMap.MapIcon.SIZE_HEIGHT_DEFAULT);
      const options: BMap.IconOptions = {};

      if (mapIcon.anchor) {
        options.anchor = new BMap.Size(mapIcon.anchor.x, mapIcon.anchor.y);
      }

      if (mapIcon.origin) {
        options.imageOffset = new BMap.Size(mapIcon.origin.x, mapIcon.origin.y);
      }

      const icon = new BMap.Icon(mapIcon.path, size, options);
      marker.setIcon(icon);
    }
    catch (e) {
      console.warn(e.message);
    }
  }

  public addMarkerEventListener(marker: BaiduMapExtendedMarker, event: string, callback: (...args: any[]) => void): void {
    marker.addEventListener(event, callback);
  }

  public isMarkerVisible(marker: any): boolean {
    return true;
  }

  public setInfoWindowContent(content: string | HTMLElement): void {
    this.infoWindow.setContent(content);
  }

  public openInfoWindow(anchor: BMap.Point | BMap.Marker | MapPointInterface): void {
    if ('getPosition' in anchor) {
      anchor = anchor.getPosition();
    }

    if (!('equals' in anchor)) {
      anchor = new BMap.Point(anchor.lng, anchor.lat);
    }

    this.map.openInfoWindow(this.infoWindow, anchor as BMap.Point);
  }

  public closeInfoWindow(): void {
    this.map.closeInfoWindow();
  }

  public addInfoWindowEventListener(event: string, callback: (...args: any[]) => void): void {
    this.infoWindow.addEventListener(event, callback);
  }

}
