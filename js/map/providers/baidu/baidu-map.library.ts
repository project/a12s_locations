(function(Drupal, drupalSettings, DrupalMap) {

  const scripts = document.getElementsByTagName('script');
  const queryString = scripts[scripts.length - 1].src;
  let baiduMapKey = /^.+[?&]key=(?<key>[^&\s?]+)(&|$)/.exec(queryString)?.groups?.key

  /**
   * Drupal behavior for Baidu map.
   *
   * This is required as Baidu map does not provide a callback when the library
   * is loaded. So we manage this manually.
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.a12sLocationsBaiduMap = {

    initialized: false,

    /**
     * Attach Drupal behaviors.
     *
     * @param context Element The current execution context
     */
    attach: function (context) {
      if (!this.initialized) {
        baiduMapKey = baiduMapKey || drupalSettings.a12sLocations?.baiduMapKey;

        if (baiduMapKey) {
          this.initialized = true;

          if (window.BMap === undefined || window.BMap.Map === undefined) {
            window.BMAP_PROTOCOL = "https";
            window.BMap_loadScriptTime = (new Date).getTime();
            DrupalMap.Map.loadScriptAsync('https://sapi.map.baidu.com/getscript?s=1&v=3.0&ak=' + baiduMapKey)
              .then(() => {
                DrupalMap.Map.init(context);
              });
          }
        }
      }
    }

  }

})(window.Drupal, window.drupalSettings, window.DrupalMap);

