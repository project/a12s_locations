import {BaseMapsSettings, SubscriberCallback} from "../types";

export default class Subscriber {

  readonly namespace: string;

  readonly mapConstructor: string;

  protected initCallbacks: Function[] = [];

  constructor(namespace: string = 'simpleMaps', mapConstructor = 'SimpleMap') {
    this.namespace = namespace;
    this.mapConstructor = mapConstructor;
  }

  addInitCallback(callback: SubscriberCallback): void {
    this.initCallbacks.push(callback);
  }

  initAllMaps(context?: Document|DocumentFragment|Element): void {
    if (!window.DrupalMap.modules.hasOwnProperty(this.mapConstructor) || typeof window.DrupalMap.modules[this.mapConstructor] !== 'function') {
      return;
    }

    const MapConstructor = window.DrupalMap.modules[this.mapConstructor];
    const a12sLocationsSettings = window.drupalSettings.a12sLocations || {};

    if (this.namespace in a12sLocationsSettings && typeof a12sLocationsSettings[this.namespace] === 'object') {
      const settings = a12sLocationsSettings[this.namespace] as {
        [key: string]: BaseMapsSettings
      };

      for (const [key, mapSettings] of Object.entries(settings)) {
        if ('initialized' in mapSettings && mapSettings.initialized === true) {
          continue;
        }

        if (!mapSettings || !('selector' in mapSettings) || !('provider' in mapSettings)) {
          console.warn(`The map settings for the key "${key}" are not valid.`);
          delete settings[key];
          continue;
        }

        const provider = mapSettings.provider;

        if (!window.DrupalMap.providers?.hasOwnProperty(provider) || !(typeof window.DrupalMap.providers[provider].Map === 'function')) {
          console.warn(`The map provider "${provider}" is missing.`);
          delete settings[key];
          continue;
        }

        if (!window.DrupalMap.providers[provider].Map.providerIsReady()) {
          // Do not delete the settings, as the provider may still be loading and
          // could be initialized after.
          continue;
        }

        const options = MapConstructor.buildOptions(mapSettings.options || {});
        const elements = window.once('a12s-locations-map', mapSettings.selector, context);

        if (elements.length) {
          if (window.DrupalMap.instances.get(elements[0]) !== undefined) {
            // We can delete this setting, as another setting already registered
            // a map for this same HTML element.
            delete settings[key];
            continue;
          }

          // Stop initialization if any callback returns false.
          const initResult = this.initCallbacks.find((callback) => {
            const result = callback(this.namespace, elements[0], options, mapSettings);
            return typeof result === 'boolean' && result === false;
          });

          if (typeof initResult !== 'undefined') {
            console.warn(`The map initialization was cancelled by an init callback.`);
            delete settings[key];
            continue;
          }

          try {
            let element = elements[0];

            // Find the HTML element containing the map if applicable.
            if ('map' in options.selectors && typeof options.selectors.map === 'string') {
              element =  elements[0].querySelector(options.selectors.map);
            }

            if (!(element instanceof HTMLElement)) {
              console.warn(`Impossible to find the map element for the key "${key}".`);
              delete settings[key];
              continue;
            }

            const map = new window.DrupalMap.providers[provider].Map(element, options.map || {});
            const mapModule = new MapConstructor(elements[0], map, options);
            window.DrupalMap.instances.set(elements[0], mapModule);
            mapModule.dispatchEvent('createdA12sDrupalMap', {
              namespace: this.namespace
            });
          }
          catch (e) {
            console.warn(`Impossible to initialize the map: ${e.message}`);
          }
          finally {
            if (key in settings) {
              settings[key].initialized = true;
            }
          }
        }
      }
    }
  }

}
