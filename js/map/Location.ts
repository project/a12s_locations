import {
  BaseMapInterface,
  DrupalMapLocationEvent,
  ExtendedMarker,
  LocationData,
  LocationMarkerOptions,
  MapIconInterface,
  MapPointInterface, SimpleMapInterface
} from "../types";

export default class Location {

  static readonly LOCATION_DATA_ATTRIBUTE_DEFAULT: string = 'a12sLocationsPoint';

  protected element: HTMLElement;

  protected data: LocationData;

  private options: LocationMarkerOptions;

  readonly map: SimpleMapInterface;

  readonly mapPoint: MapPointInterface;

  protected _marker: any;

  private _enabled: boolean = true;

  constructor(element: Element, map: SimpleMapInterface, options?: LocationMarkerOptions|null) {
    if (!(element instanceof HTMLElement)) {
      element.remove();
      throw new Error('The location element is not an HTML element.');
    }

    this.options = options || {};
    this.data = Location.parseLocationData(element, this.options.dataAttribute || window.DrupalMap.Location.LOCATION_DATA_ATTRIBUTE_DEFAULT, true);
    this.element = element;
    this.map = map;

    try {
      this.mapPoint = new window.DrupalMap.MapPoint(this.data.coordinates);
    }
    catch (e) {
      element.remove();
      throw new Error(`Point Of Sales element with ID "${this.data.id}": ${e.message}`);
    }

    this.map.dispatchEvent('alterA12sDrupalMapLocation', {
      data: this.data,
      markerOptions: this.options
    });

    if ('enabled' in this.data) {
      this._enabled = !!this.data.enabled;
    }

    this._marker = this.map.mapObject.addMarker(this, this.options, this._enabled);

    this.updateState({
      type: 'createMarker',
      locationIsActive: false
    });

    if (!this._enabled) {
      this.element.hidden = true;
    }
  }

  get marker(): any {
    return this._marker;
  }

  get enabled(): boolean {
    return this._enabled;
  }

  set enabled(enabled) {
    if (enabled !== this._enabled) {
      this._enabled = enabled;

      if (!this._enabled) {
        this.element.hidden = true;
        this.map.mapObject.disableMarker(this);
      }
      else {
        this.element.hidden = this.map.getOption('hideNotVisibleLocations') ? !(this.map.mapObject.isMarkerVisible(this.marker)) : false;
        this.map.mapObject.enableMarker(this);
      }
    }
  }

  get HTMLElement(): HTMLElement {
    return this.element;
  }

  get locationId(): string {
    return this.data.id;
  }

  get locationData(): LocationData {
    return this.data;
  }

  public addMarkerEventListener(event: string, callback: (this: any & ExtendedMarker, ...args: any[]) => void): void {
    this.map.mapObject.addMarkerEventListener(this.marker, event, function(...args) {
      // "this" should point to the target marker. If so, we can forward to the
      // callback function.
      if (this.hasOwnProperty('a12sLocation')) {
        const location = this.a12sLocation;

        if (location) {
          callback.apply(this, args);
          return;
        }
      }

      console.warn(`Trying to use a location callback for event "${event}" on an object which is not an extended map marker.`);
    });
  }

  public setMarkerIcon(icon: MapIconInterface): void {
    this.map.mapObject.setMarkerIcon(this.marker, icon);
  }

  /**
   *
   * Event types:
   * - mouseOverLocationElement
   * - mouseOutLocationElement
   * - disableActiveMarker
   * - mouseOverMarker
   * - mouseOutMarker
   * - enableActiveMarker
   *
   * @todo replace this by a publisher/subscriber system.
   */
  updateState(event: Partial<DrupalMapLocationEvent>): string|null {
    const iconCallback = this.options.markerOptions?.iconCallback || null;

    if (typeof iconCallback === "function") {
      event = Object.assign({ type: 'unknown', locationIsActive: false }, event, { location: this });
      iconCallback(event as DrupalMapLocationEvent);
    }

    return null;
  }

  /**
   * Parses the location element data.
   *
   * @param {any} element - The location element to parse.
   * @param {string} dataAttribute - The data attribute name to look for data.
   * @param {boolean} throwError - Whether to throw an error if the data is invalid. Default is false.
   *
   * @returns {LocationData|undefined} - The parsed LocationData object or undefined if the data is invalid.
   * @throws {Error} - Error message if throwError is true and the data is missing, malformed, or invalid.
   */
  public static parseLocationData(element: any, dataAttribute: string = 'locationMarker', throwError: boolean = false): LocationData|undefined {
    if (!(element instanceof HTMLElement) || !(dataAttribute in element.dataset) || !element.dataset[dataAttribute]) {
      if (throwError) {
        throw new Error('Missing marker data for the location element.');
      }
    }
    else {
      let data;

      try {
        data = JSON.parse(element.dataset[dataAttribute]);
      }
      catch (e) {
        if (throwError) {
          throw new Error('Malformed JSON marker data for the Point Of Sales element.');
        }
      }

      if (data && (typeof data.coordinates === 'undefined' || typeof data.id === 'undefined')) {
        if (throwError) {
          throw new Error('Invalid marker data for the Point Of Sales element.');
        }

        data = undefined;
      }
      else if (typeof data.id === 'number') {
        data.id = data.id.toString();
      }

      return data;
    }
  }

}
