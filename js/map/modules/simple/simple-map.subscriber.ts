(function(Drupal, DrupalMap) {

  const simpleMapSubscriber = new DrupalMap.Subscriber();
  DrupalMap.Map.subscribe(simpleMapSubscriber);

  /**
   * Drupal behavior for Simple Map.
   *
   * Ensure maps can be rendered after initial subscriber callback has been
   * called. This is required for example for AJAX requests.
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.a12sLocationsSimpleMap = {

    /**
     * Attach Drupal behaviors.
     *
     * @param context Element The current execution context
     */
    attach: function (context) {
      simpleMapSubscriber.initAllMaps(context);
    }

  }

})(window.Drupal, window.DrupalMap);
