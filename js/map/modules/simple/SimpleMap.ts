import {
  DrupalMapOptions,
  BaseMapInterface,
  DrupalMapOptionsSelectors
} from "../../../types";

export default class SimpleMap {

  protected readonly root: HTMLElement;

  protected options: DrupalMapOptions|undefined;

  readonly mapObject: BaseMapInterface;

  static readonly SELECTORS_DEFAULT: DrupalMapOptionsSelectors = {};

  /**
   * Construct a new map module.
   *
   * @param {HTMLElement} root The root HTML element, defining the map...
   * @param {BaseMapInterface} map The map object, depending on the provider ("google", "baidu"...).
   * @param {DrupalMapOptions} options
   */
  constructor(root: HTMLElement, map: BaseMapInterface,  options: DrupalMapOptions) {
    this.mapObject = map;
    this.root = root;
    this.options = options;

    if ('markers' in options && options.markers.length) {
      options.markers.forEach((markerInfo) => {
        const point = new window.DrupalMap.MapPoint(markerInfo.lat, markerInfo.lng);
        const marker = this.mapObject.addMarker(point);

        if ('options' in markerInfo && 'icon' in markerInfo.options) {
          const icon = window.DrupalMap.MapIcon.buildFromIconData(markerInfo.options.icon);
          this.mapObject.setMarkerIcon(marker, icon);
        }
      });
    }

  }

  public static buildOptions(options?: Partial<DrupalMapOptions>): DrupalMapOptions {
    options = options || {};
    options.selectors = Object.assign(this.SELECTORS_DEFAULT, options.selectors || {});
    return options as DrupalMapOptions;
  }

  getOptions(): DrupalMapOptions {
    return this.options;
  }

  getOption(key: string, defaultValue: any = undefined): any {
    return this.options.hasOwnProperty(key) ? this.options[key] : defaultValue;
  }

  dispatchEvent(type: string, data?: { [key: string]: any }) {
    const details = data || {};
    details.map = this;

    const event = new CustomEvent(type, {
      detail: details
    });

    this.root.dispatchEvent(event);
  }

}
