import {LocationInterface, MapPointInterface} from "../../../types";

/**
 * Represents a list of Point of Sales markers.
 */
export class LocationList extends Map<HTMLElement, LocationInterface> {

  /**
   * Constructs a new instance of LocationList.
   *
   * @constructor
   */
  constructor() {
    super();
  }

  /**
   * Returns the map points.
   *
   * @return {MapPointInterface[]} - An array containing the map points.
   */
  getMapPoints(): MapPointInterface[] {
    return [...this.values()].reduce((mapPoints, location) => {
      if (location.enabled) {
        mapPoints.push(location.mapPoint);
      }

      return mapPoints;
    }, [] as MapPointInterface[]);
  }

  /**
   * Returns the markers.
   *
   * @return {any[]} - An array containing the markers.
   */
  getMarkers(): any[] {
    return [...this.values()].reduce((markers, location: LocationInterface) => {
      if (location.enabled) {
        markers.push(location.marker);
      }

      return markers;
    }, [] as any[]);
  }

}
