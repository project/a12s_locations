import {
  a12sLocationsMapUpdateCommand,
  LocationMapInterface
} from "../../../types";

(function(Drupal, DrupalMap) {

  /**
   * Update the enabled location on map.
   *
   * @param {Drupal.Ajax} [ajax]
   *   AJAX object created by {@link Drupal.ajax}.
   * @param {a12sLocationsMapUpdateResponse} response
   *   The JSON response from the Ajax request.
   * @param {number} [status]
   *   The XMLHttpRequest status.
   */
  Drupal.AjaxCommands.prototype.a12sLocationsMapUpdate = function (ajax, response, status) {
    if ('selector' in response) {
      const mapRoot = document.querySelector(response.selector);

      if (mapRoot instanceof HTMLElement && 'enabledLocations' in response) {
        const locationMap = DrupalMap.instances.get(mapRoot) as LocationMapInterface;

        if (locationMap && 'setEnabledLocations' in locationMap) {
          locationMap.setEnabledLocations(response.enabledLocations);
        }
      }
    }
  } as a12sLocationsMapUpdateCommand;

})(window.Drupal, window.DrupalMap);
