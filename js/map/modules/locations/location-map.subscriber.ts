import {LocationMapsSettings} from "../../../types";

(function(Drupal, DrupalMap) {

  const locationMapSubscriber = new DrupalMap.Subscriber('locationMaps', 'LocationMap');
  locationMapSubscriber.addInitCallback((namespace, element, options, mapSettings: LocationMapsSettings) => {
    // Filter the location if necessary.
    if (mapSettings.hasOwnProperty('enabledLocations') && mapSettings.enabledLocations) {
      const locationListContainer = element.querySelector(options.selectors.locationList);

      if (locationListContainer) {
        const dataAttribute = 'data-' + options.location.dataAttribute.split(/\.?(?=[A-Z])/).join('-').toLowerCase();

        locationListContainer.querySelectorAll(options.selectors.locationElement).forEach((element: HTMLElement) => {
          const data = DrupalMap.Location.parseLocationData(element, options.location.dataAttribute || DrupalMap.Location.LOCATION_DATA_ATTRIBUTE_DEFAULT);

          if (data) {
            data.enabled = 'id' in data && mapSettings.enabledLocations.includes(data.id);
            element.setAttribute(dataAttribute, JSON.stringify(data));
          }
        });
      }
    }

    return true;
  });

  DrupalMap.Map.subscribe(locationMapSubscriber);

  /**
   * Drupal behavior for Location Map.
   *
   * Ensure maps can be rendered after initial subscriber callback has been
   * called. This is required for example for AJAX requests.
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.a12sLocationsLocationMap = {

    /**
     * Attach Drupal behaviors.
     *
     * @param context Element The current execution context
     */
    attach: function (context) {
      locationMapSubscriber.initAllMaps(context);
    }

  }

})(window.Drupal, window.DrupalMap);

