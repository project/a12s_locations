import {LocationList} from "./LocationList";
import {
  DrupalMapOptions,
  BaseMapInterface,
  DrupalMapOptionsSelectors,
  ExtendedMarker,
  LocationInterface
} from "../../../types";

export default class LocationMap extends window.DrupalMap.modules.SimpleMap {

  readonly locationListContainer: HTMLElement;

  readonly locationList: LocationList;

  protected currentSearch: null | string;

  public locationActive?: LocationInterface;

  static readonly SELECTORS_DEFAULT: DrupalMapOptionsSelectors = {
    map: '[data-a12s-locations-map]',
    locationList: '[data-a12s-locations-list]',
    locationElement: '[data-a12s-locations-point]',
  };

  /**
   * Construct a new Store Locator map object.
   *
   * @param {HTMLElement} root The root HTML element, defining the map...
   * @param {BaseMapInterface} map The map object, depending on the provider ("google", "baidu"...).
   * @param {Partial<DrupalMapOptions>} options
   */
  constructor(root: HTMLElement, map: BaseMapInterface,  options: DrupalMapOptions) {
    super(root, map,  options);
    options.hideNotVisibleLocations = options.hideNotVisibleLocations || false;
    options.zoomOnLocationClick = options.zoomOnLocationClick || undefined;

    this.currentSearch = null;
    this.locationListContainer = root.querySelector(options.selectors.locationList);
    this.locationList = new LocationList();

    if (this.locationListContainer) {
      this.locationListContainer.querySelectorAll(options.selectors.locationElement).forEach((element) => {
        // We can safely cast the "element" to HTMLElement, as the Location
        // class will throw an error if it is not of such type.
        this.createLocation(element as HTMLElement);
      });

      this.locationListContainer.addEventListener('click', (event) => {
        const mapPoint = this.getLocationFromLocationEvent(event)?.mapPoint;

        if (mapPoint) {
          if (this.options.zoomOnLocationClick !== undefined) {
            this.mapObject.setZoom(this.options.zoomOnLocationClick);
          }

          this.mapObject.setCenter(mapPoint);
          this.disableActiveLocation();
          this.mapObject.closeInfoWindow();
        }
      });

      this.locationListContainer.addEventListener('mouseover', (event) => {
        const location = this.getLocationFromLocationEvent(event);

        location?.updateState({
          locationIsActive: this.locationActive === location,
          type: 'mouseOverLocationElement'
        });
      });

      this.locationListContainer.addEventListener('mouseout', (event) => {
        const location = this.getLocationFromLocationEvent(event);

        location?.updateState({
          locationIsActive: this.locationActive === location,
          type: 'mouseOutLocationElement'
        });
      });
    }

    if (options.hideNotVisibleLocations) {
      this.mapObject.addMapEventListener('bounds_changed', () => {
        this.locationList.forEach((location) => {
          if (location.enabled) {
            location.HTMLElement.hidden = !(this.mapObject.isMarkerVisible(location.marker));
          }
        });
      });
    }

    this.mapObject.addInfoWindowEventListener('close', () => {
      this.disableActiveLocation();
    });

    this.mapObject.initMarkerClusterer();
    this.mapObject.addMarkersToClusterer(this.locationList.getMarkers());
    this.dispatchEvent('initializedA12sLocationsMap');
  }

  public static buildOptions(options?: Partial<DrupalMapOptions>): DrupalMapOptions {
    options = super.buildOptions(options);
    options.location = Object.assign({ dataAttribute: window.DrupalMap.Location.LOCATION_DATA_ATTRIBUTE_DEFAULT }, options.location || {});
    return options as DrupalMapOptions;
  }

  disableActiveLocation() {
    if (this.locationActive) {
      this.locationActive.updateState({
        locationIsActive: false,
        type: 'disableActiveMarker'
      });
      this.locationActive.HTMLElement.classList.remove('active');
    }

    this.locationActive = undefined;
  }

  protected createLocation(element: HTMLElement): void {
    const locationMap = this;

    try {
      const location = new window.DrupalMap.Location(element, this, this.options.location || {});
      this.locationList.set(element, location);

      location.addMarkerEventListener('mouseover', function(this: any & ExtendedMarker) {
        location.HTMLElement.classList.add('active');
        location.updateState({ locationIsActive: locationMap.locationActive === this.a12sLocation, type: 'mouseOverMarker' });
      });

      location.addMarkerEventListener('mouseout', function(this: any & ExtendedMarker) {
        if (locationMap.locationActive !== this.a12sLocation) {
          location.HTMLElement.classList.remove('active');
          location. updateState({ locationIsActive: locationMap.locationActive === this.a12sLocation, type: 'mouseOutMarker' });
        }
      });

      location.addMarkerEventListener('click', function(this: any & ExtendedMarker) {
        if (locationMap.locationActive === this.a12sLocation) {
          locationMap.mapObject.closeInfoWindow();
          locationMap.locationActive.HTMLElement.classList.remove('active');
          locationMap.locationActive = undefined;
          location.updateState({ locationIsActive: locationMap.locationActive === this.a12sLocation, type: 'disableActiveMarker' });
        }
        else {
          const locationContent = document.createElement('div');
          locationContent.classList.add('location-info-window');
          locationContent.innerHTML = this.a12sLocation.HTMLElement.innerHTML;

          if (locationMap.locationActive) {
            locationMap.locationActive.updateState({ locationIsActive: false, type: 'disableActiveMarker' });
            locationMap.locationActive.HTMLElement.classList.remove('active');
          }

          locationMap.mapObject.setInfoWindowContent(locationContent);
          locationMap.mapObject.openInfoWindow(this);
          locationMap.locationActive = this.a12sLocation;
          location.updateState({ locationIsActive: true, type: 'enableActiveMarker' });

          const containerPos = locationMap.locationListContainer.getBoundingClientRect(),
            locationPos = this.a12sLocation.HTMLElement.getBoundingClientRect();
          locationMap.locationListContainer.scrollTop = locationPos.top - containerPos.top + locationMap.locationListContainer.scrollTop;
        }
      });
    }
    catch (error) {
      //element.remove();
      console.log(`Invalid location: ${error.message}`);
    }
  }

  getLocationFromLocationEvent(event: Event): LocationInterface|undefined {
    if (event.target && event.target instanceof Element) {
      const locationElement = event.target.closest(this.options.selectors.locationElement);

      if (locationElement instanceof HTMLElement) {
        return this.locationList.get(locationElement);
      }
    }
  }

  setEnabledLocations(locationIds: string[]) {
    this.locationList.forEach((location) => {
      location.enabled = locationIds.includes(location.locationId);
    });

    this.rebuildCluster();
    this.dispatchEvent('updateEnabledA12sLocations');
  }

  rebuildCluster() {
    this.mapObject.clearMarkersFromClusterer();
    this.mapObject.addMarkersToClusterer(this.locationList.getMarkers());
  }

}
