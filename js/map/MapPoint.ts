import {LatLng} from "../types";

/**
 * A Map Point gathers geographical coordinates: latitude and longitude.
 *
 * Latitude ranges between -90 and 90 degrees, inclusive. Values above or below
 * this range will be clamped to the range [-90, 90].
 *
 * Longitude ranges between -180 and 180 degrees, inclusive. Values above or
 * below this range will be wrapped so that they fall within the range.
 */
export default class MapPoint {

  readonly lat: number;

  readonly lng: number;

  /**
   * Constructs a new MapPoint object with the given latitude and longitude.
   *
   * The latitude and longitude can be provided either as separate parameters or
   * as a LatLng object.
   *
   * @param {number | string | LatLng | MapPoint} latOrLatLng - The latitude of
   *   the MapPoint, in degrees. Can be a number or a string representation of
   *   coordinates (e.g. "5.456,-12.845"). Alternatively, a LatLng or MapPoint
   *   object can be provided.
   * @param {number | null} lng - The longitude of the MapPoint, in degrees.
   *   Only required if the first parameter is a number.
   *
   * @throws {Error} - If the argument types are not valid.
   */
  constructor(latOrLatLng: number | string | LatLng | MapPoint, lng?: number | null) {
    if (typeof latOrLatLng === 'number') {
      if (typeof lng !== 'number') {
        throw Error('MapPoint require either a latitude and a longitude.');
      }

      this.lat = latOrLatLng;
      this.lng = lng;
    }
    else if (typeof latOrLatLng === 'string') {
      const [latString, lngString = ''] = latOrLatLng.split(',');
      this.lat = parseFloat(latString);
      this.lng = parseFloat(lngString);

      if (isNaN(this.lat) || isNaN(this.lng)) {
        throw Error('The provided string does not contain valid coordinates.');
      }
    }
    else {
      if (typeof latOrLatLng !== 'object') {
        throw Error('The arguments provided to Map Point are not valid.');
      }

      if (!latOrLatLng.hasOwnProperty('lat') || typeof latOrLatLng.lat !== 'number') {
        throw Error('The provided latitude is missing or not valid.');
      }

      if (!latOrLatLng.hasOwnProperty('lng') || typeof latOrLatLng.lng !== 'number') {
        throw Error('The provided longitude is missing or not valid.');
      }

      this.lat = latOrLatLng.lat;
      this.lng = latOrLatLng.lng;
    }

    if (this.lat < -90) {
      this.lat = -90;
    }

    if (this.lat > 90) {
      this.lat = 90;
    }

    if (this.lng < -180) {
      this.lng = -180;
    }

    if (this.lng > 180) {
      this.lng = 180;
    }
  }

}
