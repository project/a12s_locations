declare namespace drupal {

  namespace Core {

    interface IBehavior {

      attach(
        context?: HTMLElement,
        settings?: IDrupalSettings
      ): void;

      detach?(
        context?: HTMLElement,
        settings?: IDrupalSettings,
        trigger?: string
      ): void;

    }

    interface IBehaviors {
      [key: string]: IBehavior
    }

    interface ITranslationOptions {

      context?: string;

    }

    interface IPlaceholders {
      [key: string]: string;
    }

    interface ITheme {

      (func: 'placeholder', str: string): string;

      (func: string, ...params: any[]): string;

    }

    interface IUrlGenerator {

      (path: string): string;

      toAbsolute(url: string): string;

      isLocal(url: string): boolean;

    }

    interface ILocale {
    }
  }

  interface IDrupalSettings {

    // @todo Move into an Interface.
    path: {

      baseUrl: string;

      currentLanguage: string;

      currentPath: string;

      currentPathIsAdmin: string;

      isFront: boolean;

      pathPrefix: string;

      scriptPath: string;

    };

    pluralDelimiter: string;

    // @todo Move into an Interface.
    user: {

      uid: number;

      permissionsHash: string;

    };

  }

  interface IDrupalStatic {

    attachBehaviors(
      context: HTMLElement,
      settings: IDrupalSettings
    ): void;

    detachBehaviors(
      context?: HTMLElement,
      settings?: IDrupalSettings,
      trigger?: string
    ): void;

    behaviors: Core.IBehaviors;

    locale: Core.ILocale;

    checkPlain(text: string): string;

    // @todo Remove any.
    checkWidthBreakpoint?(width: number): any;

    // @todo Remove any.
    encodePath(item: any): any;

    formatPlural(
      count: number,
      singular: string,
      plural: string,
      args?: Core.IPlaceholders,
      options?: Core.ITranslationOptions
    ): string;

    formatString(
      str: string,
      args: Core.IPlaceholders
    ): string;

    stringReplace(
      str: string,
      args?: Core.IPlaceholders,
      keys?: string[]
    ): string;

    t(
      str: string,
      args?: Core.IPlaceholders,
      options?: Core.ITranslationOptions
    ): string;

    theme: Core.ITheme;

    throwError(error: Error): void;

    url: Core.IUrlGenerator;

  }
}

/* tslint:disable:interface-name */
interface Window {

  Drupal?: drupal.IDrupalStatic;

  drupalSettings?: drupal.IDrupalSettings;

  once(id: string, selector: NodeList|Array<Element>|Element|string, context?: Document|DocumentFragment|Element): Array<HTMLElement>;

}
/* tslint:enable:interface-name */

declare var drupalSettings: drupal.IDrupalSettings;
declare var Drupal: drupal.IDrupalStatic;
