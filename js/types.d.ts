import IAjaxCommand = drupal.Core.IAjaxCommand;
import IAjax = drupal.Core.IAjax;
import Location from "./map/Location";
import SimpleMap from "./map/modules/simple/SimpleMap";
import LocationMap from "./map/modules/locations/LocationMap";
import BaseMap from "./map/providers/BaseMap";
import MapPoint from "./map/MapPoint";
import MapIcon from "./map/MapIcon";
import Subscriber from "./map/Subscriber";

export type BaseMapInterface = BaseMap;

export type SimpleMapInterface = SimpleMap;

interface BaseMapAbstractInterface {
  providerIsReady(): boolean
  subscribe(callback: Function): void
  init(context?: Document|DocumentFragment|Element): void
  loadScriptAsync(src: string): Promise<boolean>
}

export type LocationInterface = Location;

export type LocationMapInterface = LocationMap;

export type MapPointInterface = MapPoint;

export type MapIconInterface = MapIcon;

export type SubscriberInterface = Subscriber;

export type SubscriberCallback = (namespace: string, element: HTMLElement, options: DrupalMapOptions, mapSettings: BaseMapsSettings) => boolean|null;

export interface ExtendedMarker {
  a12sLocation?: LocationInterface
}

/**
 * The data related to a given location.
 */
export interface LocationData {

  id: string;

  title?: string;

  coordinates: string;

  address?: string;

  city?: string;

  postalCode?: string;

  [key: string]: any;

}

export interface LocationMarkerOptions {

  dataAttribute?: string

  markerOptions?: DrupalMapMarkerOptions

}

export interface LatLng {

  /**
   * Latitude in degrees.
   */
  lat: number;

  /**
   * Longitude in degrees.
   */
  lng: number;

}

export interface DrupalMapEvent {

  type: string

}

export interface DrupalMapLocationEvent extends DrupalMapEvent {

  location: Location

  locationIsActive: boolean

}

export interface DrupalMapOptionsSelectors {

  [key: string]: string

  map?: string

  locationList?: string

  locationElement?: string

}

export interface DrupalMapBaseMapSettings {

  /**
   * Specific settings related to a provider.
   */
  provider?: any

  infoWindowOptions?: any

  center?: {
    lat: number
    lng: number
  }

  style?: any

  clusterOptions?: any

  /**
   * The initial zoom level.
   */
  initialZoom?: number

  /**
   * The minimum zoom level.
   */
  minZoom?: number

  /**
   * The maximum zoom level.
   */
  maxZoom?: number

}

export interface DrupalMapMarkerOptions {

  iconCallback?: (event: DrupalMapLocationEvent) => void

}

export interface DrupalMapMarkerInfo {

  lat: number

  lng: number

  [key: string]: any

}

export interface DrupalMapOptions {

  [key: string]: any

  selectors: DrupalMapOptionsSelectors

  hideNotVisibleLocations?: boolean

  zoomOnLocationClick?: number

  location?: LocationMarkerOptions

  map?: DrupalMapBaseMapSettings

  markers?: DrupalMapMarkerInfo[]

}

export interface DrupalMapInterface {

  /**
   * Create a subscriber to initialize maps of a given type.
   *
   * @class Subscriber
   * @constructor
   */
  Subscriber: typeof Subscriber

  /**
   * Abstract base class for a Map of a specific provider.
   *
   * Defines a list of methods that should be implemented by child classes. It
   * is used as an abstraction to ensure compatibility between tha various map
   * providers.
   *
   * @class BaseMap
   */
  Map: typeof BaseMap

  /**
   * Represents a point on a map.
   *
   * @cmass MapPoint
   *
   * @property {number} latitude - The latitude of the map point.
   * @property {number} longitude - The longitude of the map point.
   */
  MapPoint?: typeof MapPoint

  /**
   * Represents a location.
   *
   * A location is an HTML element with JSON data stored in a predefined data
   * attribute, containing at least the properties:
   * - id: A unique identifier.
   * - coordinates: The location coordinates in a format supported by the
   *   MapPoint class constructor.
   *
   * The HTML content of the location element is used by the LocationMap module
   * for the information window associated with the marker.
   *
   * @class Location
   */
  Location?: typeof Location

  /**
   * Represents a MapIcon.
   *
   * The icon can be created from a remote URL or a base64 encoded string.
   *
   * @class MapIcon
   */
  MapIcon?: typeof MapIcon

  /**
   * Stores all initialized map instances, keyed by their root element.
   *
   * Each map instance is a SimpleMap object or an instance of a child class.
   *
   * @type {Map<HTMLElement, SimpleMapInterface>}
   */
  instances?: Map<HTMLElement, SimpleMapInterface>

  /**
   * A provider is a specialized class implementing the BaseMap class.
   *
   * When applicable, a provider ca also expose a Clusterer constructor to
   * create groups of markers.
   */
  providers?: {
    [key: string]: {
      Map: BaseMapAbstractInterface & {
        new(element: HTMLElement, settings: DrupalMapBaseMapSettings): BaseMapInterface
      }

      MarkerClusterer?: new(...args: any[]) => any
    }
  }

  /**
   * A module is a type of map with specific features.
   *
   * The native modules are:
   * - SimpleMap: A simple map displaying markers.
   * - LocationMap: An advanced map based on a list of locations, which are
   *   parsed on initialization to generate the markers displayed on the map.
   *   The module provides:
   *   - Information window.
   *   - Auto-zoom when a location is clicked.
   *   - Grouping of markers using Clusterer, when applicable.
   */
  modules?: {

    [key: string]: typeof SimpleMap

    SimpleMap?: typeof SimpleMap

    LocationMap?: typeof LocationMap
  }

  /**
   * Retrieves the root element from the given settings.
   *
   * @param {Object} settings - The settings for the map.
   * @param {string} settings.selector - The CSS selector of the root element.
   *
   * @return {HTMLElement|undefined} - The root element of the map, or undefined if not found.
   */
  getMapRootElement(settings: any): HTMLElement|undefined;

  /**
   * Retrieves the map module asynchronously.
   *
   * @param {HTMLElement} mapRoot - The root element where the map will be mounted.
   *
   * @return {Promise<SimpleMapInterface>} - A Promise that resolves to the SimpleMapInterface.
   */
  getMapModuleAsync(mapRoot: HTMLElement): Promise<SimpleMapInterface>

}

export interface BaseMapsSettings {

  selector: string

  provider: string

  options?: Partial<DrupalMapOptions>

  initialized?: boolean

}

export interface SimpleMapsSettings extends BaseMapsSettings {

  markers?: string[]

}

export interface LocationMapsSettings extends BaseMapsSettings {

  enabledLocations?: string[]

}

declare namespace drupalMap {

  namespace Icon {

    export interface Size {
      height: number
      width: number
    }

    export interface Position {
      x: number
      y: number
    }

    export interface Options {

      size?: drupalMap.Icon.Size

      anchor?: drupalMap.Icon.Position

      origin?: drupalMap.Icon.Position

    }

  }

}

export interface a12sLocationsMapUpdateCommand extends IAjaxCommand {
  (
    ajax: IAjax,
    response: a12sLocationsMapUpdateResponse,
    status: number
  ): void;
}

interface a12sLocationsMapUpdateResponse {
  data: any
  enabledLocations: string[]
  selector: string
}
