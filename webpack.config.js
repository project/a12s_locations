const path = require('path');
const TerserPlugin = require("terser-webpack-plugin");

const config = [];

function standaloneLibrary(src, subPath, defaultExport = false, externals = null) {
  const baseMapConfig = {
    entry: {},
    output: {
      path: path.resolve(__dirname),
      filename: '[name].js',
      library: ['DrupalMap'],
      libraryTarget: 'umd',
      libraryExport: 'default',
    },
    optimization: {
      minimize: (process.env.NODE_ENV === 'production'),
    },
    devtool: 'source-map',
    module: {
      rules: [
        {
          test: /\.ts$/,
          exclude: /node_modules/,
          use: ['babel-loader', 'ts-loader']
        }
      ]
    },
    resolve: {
      extensions: ['.ts', '.js']
    },
  };

  baseMapConfig.entry[src] = path.resolve(__dirname, './' + src + '.ts');
  baseMapConfig.output.library = baseMapConfig.output.library.concat(subPath);

  if (defaultExport) {
    baseMapConfig.output.libraryExport = 'default';
  }

  if (externals) {
    baseMapConfig.externals = externals;
  }

  config.push(baseMapConfig);
}

standaloneLibrary('js/map/DrupalMap', [], true);
standaloneLibrary('js/map/modules/locations/LocationMap', ['modules', 'LocationMap'], true);
standaloneLibrary('js/map/providers/google/MarkerClusterer', ['providers', 'google', 'MarkerClusterer'], true);
standaloneLibrary('js/map/providers/google/GoogleMap', ['providers', 'google', 'Map'], true, {
  'BaseMap': 'window.DrupalMap.map'
});
standaloneLibrary('js/map/providers/baidu/BaiduMap', ['providers', 'baidu', 'Map'], true, {
  'BaseMap': 'window.DrupalMap.map'
});

config.push({
  entry: {
    'map/modules/simple/simple-map.subscriber': path.resolve(__dirname, './js/map/modules/simple/simple-map.subscriber.ts'),
    'map/modules/locations/location-map.subscriber': path.resolve(__dirname, './js/map/modules/locations/location-map.subscriber.ts'),
    'map/modules/locations/location-map.update-ajax': path.resolve(__dirname, './js/map/modules/locations/location-map.update-ajax.ts'),
    'map/providers/baidu/baidu-map.library': path.resolve(__dirname, './js/map/providers/baidu/baidu-map.library.ts')
  },
  output: {
    path: path.resolve(__dirname, './js'),
    filename: '[name].js'
  },
  optimization: {
    minimize: (process.env.NODE_ENV === 'production'),
    minimizer: [
      new TerserPlugin({
        terserOptions: {
          mangle: {
            reserved: ['Drupal']
          }
        }
      })
    ]
  },
  externals: {
    'once': 'window.once',
    'drupalSettings': 'window.drupalSettings',
    'Drupal': 'window.Drupal',
    'DrupalMap': 'window.DrupalMap',
  },
  devtool: 'source-map',
  module: {
    rules: [
      {
        test: /\.ts$/,
        exclude: /node_modules/,
        use: ['babel-loader', 'ts-loader']
      }
    ]
  },
  resolve: {
    extensions: ['.ts', '.js']
  },
});

module.exports = config;
