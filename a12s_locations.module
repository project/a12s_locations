<?php

/**
 * @file
 * Provides specific features for A12s Locations module.
 */

use Drupal\a12s_locations\Form\LocationsMapSettingsForm;
use Drupal\a12s_locations\Plugin\views\style\LocationsMap;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityMalformedException;
use Drupal\Core\Utility\Error;
use Drupal\views\DisplayPluginCollection;
use Drupal\views\Entity\View;
use Drupal\views\Views;

/**
 * @see hook_entity_type_build()
 */
function a12s_locations_entity_type_build(array &$entity_types): void {
  if (isset($entity_types['view'])) {
    $entity_types['view']
      ->setFormClass('a12s_locations_update_cache', 'Drupal\a12s_locations\Form\CacheUpdateConfirmForm')
      ->setLinkTemplate('a12s-locations-update-cache', '/admin/structure/views/view/{view}/a12s-locations-update-cache');
  }
}

/**
 * @see hook_entity_operation()
 */
function a12s_locations_entity_operation(EntityInterface $entity): array {
  $operations = [];

  if ($entity instanceof View) {
    /** @var \Drupal\views\ViewExecutableFactory $viewFactory */
    $viewFactory = \Drupal::service('views.executable');
    $view = $viewFactory->get($entity);
    $displayHandlers = new DisplayPluginCollection($view, Views::pluginManager('display'));

    foreach ($displayHandlers as $displayHandler) {
      $stylePlugin = $displayHandler->getPlugin('style');

      if ($stylePlugin instanceof LocationsMap && $stylePlugin->getCacheOption('enabled')) {
        try {
          $operations['a12s_locations_update_cache'] = [
            'title' => t('Update cache'),
            'url' => $entity->toUrl('a12s-locations-update-cache'),
            'weight' => 50,
          ];
        }
        catch (EntityMalformedException $e) {
          Error::logException(\Drupal::logger('a12s_locations'), $e);
        }

        break;
      }
    }
  }

  return $operations;
}

// @todo add a link through hook_contextual_links_view_alter() to update the
//   cache on applicable view displays.
//   @see views_ui_contextual_links_view_alter()
// @todo add a tab on the view edit page.

/**
 * @see hook_library_info_build()
 */
function a12s_locations_library_info_build(): array {
  $libraries = [];
  $config = \Drupal::configFactory()->get(LocationsMapSettingsForm::A12S_LOCATIONS_SETTINGS);

  if ($key = $config->get('baidu_map_api_key')) {
    $libraries['baidu-map'] = [
      'js' => [
        'js/map/providers/baidu/baidu-map.library.js?key=' . $key => [
          'preprocess' => FALSE,
          'minified' => TRUE
        ],
      ],
      'dependencies' => [
        'core/drupal',
        'core/drupalSettings',
        'a12s_locations/baidu-map-base',
      ],
    ];
  }

  if ($key = $config->get('google_map_api_key')) {
    $libraries['google-map'] = [
      'js' => [
        'https://maps.googleapis.com/maps/api/js?key=' . $key . '&callback=DrupalMap.Map.init' => [
          'preprocess' => FALSE,
          'type' => 'external',
          'minified' => TRUE,
          'attributes' => [
            'async' => TRUE,
          ]
        ],
      ],
      'dependencies' => [
        'a12s_locations/google-map-base',
      ],
    ];
  }

  return $libraries;
}

/**
 * Prepares variables for views "a12s locations map" templates.
 *
 * Default template: views-view-a12s-locations-map.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - view: A ViewExecutable object.
 *   - rows: The raw row data.
 */
function template_preprocess_views_view_a12s_locations_map(array &$variables): void {
  $variables['attributes']['class'][] = 'a12s-locations-list-wrapper';
  $variables['content_attributes']['data-a12s-locations-list'] = TRUE;

  /** @var \Drupal\views\ViewExecutable $view */
  $view = $variables['view'];
  $stylePlugin = $view->style_plugin;

  if ($stylePlugin->options['class']) {
    $variables['content_attributes']['class'] = array_map(
      '\Drupal\Component\Utility\Html::cleanCssIdentifier',
      explode(' ', $stylePlugin->options['class']));
  }
}
