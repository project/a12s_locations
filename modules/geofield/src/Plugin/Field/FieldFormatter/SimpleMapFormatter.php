<?php

declare(strict_types=1);

namespace Drupal\a12s_locations_geofield\Plugin\Field\FieldFormatter;

use Drupal\a12s_locations\Form\MapConfigTrait;
use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\geofield\GeoPHP\GeoPHPInterface;
use Drupal\geofield\Plugin\Field\FieldFormatter\GeofieldDefaultFormatter;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'Simple map' formatter.
 *
 * @FieldFormatter(
 *   id = "a12s_locations_simple_map",
 *   label = @Translation("Simple map"),
 *   field_types = {"geofield"},
 * )
 */
class SimpleMapFormatter extends GeofieldDefaultFormatter {

  use MapConfigTrait;

  /**
   * {@inheritDoc}
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    $label,
    $view_mode,
    array $third_party_settings,
    GeoPHPInterface $geophp_wrapper,
    protected EntityTypeManagerInterface $entityTypeManager
  ) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings, $geophp_wrapper);
  }

  /**
   * {@inheritdoc}
   *
   * @noinspection PhpParamsInspection
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): static {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('geofield.geophp'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings(): array {
    return [
      'map_configuration' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    return $form + $this->mapProviderFormElement($this->getSetting('map_configuration'));
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(): array {
    return [
      $this->t('Map configuration: @map_configuration_id', ['@map_configuration_id' => $this->getSetting('map_configuration')]),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $element = [];

    if (!($mapProviderPlugin = $this->getPluginFromMapConfigId($this->getSetting('map_configuration')))) {
      return [];
    }

    $element['#attached']['library'][] = 'a12s_locations/simple-map';

    foreach ($mapProviderPlugin->getLibraries() as $library) {
      $element['#attached']['library'][] = $library;
    }

    $id = Html::getUniqueId('a12s-locations-simple-map-' . HTML::cleanCssIdentifier($this->fieldDefinition->getName()));
    $element['#type'] = 'container';
    $element['#attributes']['id'][] = $id;
    $element['#attributes']['class'][] = 'a12s-locations-simple-map';

    $settings = $mapProviderPlugin->buildDrupalSettings('#' . $id);

    foreach ($items as $item) {
      $geom = $this->geoPhpWrapper->load($item->value);

      if ($geom) {
        // If the geometry is not a point, get the centroid.
        // @todo Add support for lines and polygons.
        if ($geom->getGeomType() != 'Point') {
          $geom = $geom->centroid();
        }

        $settings['options']['markers'][] = [
          'lat' => $geom->y(),
          'lng' => $geom->x(),
        ];
      }
    }

    $element['#attached']['drupalSettings']['a12sLocations']['simpleMaps'][$id] = $settings;
    return $element;
  }

}
